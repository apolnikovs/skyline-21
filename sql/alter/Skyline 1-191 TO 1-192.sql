# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 

call UpgradeSchemaVersion('1.191');

# ---------------------------------------------------------------------- #
# Add table "appointment"                                                #
# ---------------------------------------------------------------------- #
ALTER TABLE appointment ADD COLUMN CustContactTime INT(3) NULL DEFAULT '60' AFTER BreakDurationSec, 
						ADD COLUMN CustContactType ENUM('None','SMS','Phone') NULL DEFAULT 'SMS' AFTER CustContactTime;


# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #

insert into version (VersionNo) values ('1.192');
