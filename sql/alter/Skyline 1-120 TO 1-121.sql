# ---------------------------------------------------------------------- #
# Script generated with: DeZign for Databases V7.2.0                     #
# Target DBMS:           MySQL 5                                         #
# Project file:          SkyLine.dez                                     #
# Project name:          SkyLine                                         #
# Author:                Brian Etherington                               #
# Script type:           Alter database script                           #
# Created on:            2012-11-12 14:24                                #
# ---------------------------------------------------------------------- #


# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                                     #
# ---------------------------------------------------------------------- # 

call UpgradeSchemaVersion('1.120');

# ---------------------------------------------------------------------- #
# Drop foreign key constraints                                           #
# ---------------------------------------------------------------------- #

ALTER TABLE `appointment` DROP FOREIGN KEY `job_TO_appointment`;

ALTER TABLE `appointment` DROP FOREIGN KEY `user_TO_appointment`;

# ---------------------------------------------------------------------- #
# Modify table "appointment"                                             #
# ---------------------------------------------------------------------- #

ALTER TABLE `appointment` MODIFY `importance` TINYINT(2) DEFAULT 0;

UPDATE `appointment` SET `importance` = 0 WHERE `importance` is null;

# ---------------------------------------------------------------------- #
# Add foreign key constraints                                            #
# ---------------------------------------------------------------------- #

ALTER TABLE `appointment` ADD CONSTRAINT `job_TO_appointment` 
    FOREIGN KEY (`JobID`) REFERENCES `job` (`JobID`);

ALTER TABLE `appointment` ADD CONSTRAINT `user_TO_appointment` 
    FOREIGN KEY (`UserID`) REFERENCES `user` (`UserID`);

# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #

insert into version (VersionNo) values ('1.121');
