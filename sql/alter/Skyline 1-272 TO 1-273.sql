# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 
call UpgradeSchemaVersion('1.272');

# ---------------------------------------------------------------------- #
# Modify table ra_status                                                 #
# ---------------------------------------------------------------------- # 
ALTER TABLE ra_status ALTER BrandID DROP DEFAULT, 
					  ALTER RAStatusCode DROP DEFAULT; 
					  
ALTER TABLE ra_status CHANGE COLUMN BrandID BrandID INT(11) NOT NULL AFTER SBAuthorisationStatusID, 
					  CHANGE COLUMN RAStatusName RAStatusName VARCHAR(40) NULL DEFAULT NULL AFTER BrandID, 
					  CHANGE COLUMN RAStatusCode RAStatusCode INT(11) NOT NULL AFTER RAStatusName, 
					  ADD COLUMN Final ENUM('Yes','No') NULL DEFAULT NULL AFTER RAStatusCode, 
					  CHANGE COLUMN Colour Colour VARCHAR(10) NULL DEFAULT NULL AFTER Final;

# ---------------------------------------------------------------------- #
# Modify table ra_history                                                #
# ---------------------------------------------------------------------- # 
ALTER TABLE ra_history CHANGE COLUMN RAType RAType ENUM('repair','return','estimate','invoice','on hold') NULL DEFAULT NULL AFTER RAStatusID, 
					   ADD COLUMN ModifiedDate TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP AFTER ModifiedUserID,
					   DROP COLUMN CreatedDateTime;



# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #
insert into version (VersionNo) values ('1.273');
