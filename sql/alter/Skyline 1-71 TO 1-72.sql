# ---------------------------------------------------------------------- #
# Script generated with: DeZign for Databases V7.1.2                     #
# Target DBMS:           MySQL 5                                         #
# Project file:          SkyLine.dez                                     #
# Project name:          SkyLine                                         #
# Author:                Brian Etherington                               #
# Script type:           Alter database script                           #
# Created on:            2012-08-15 16:36                                #
# ---------------------------------------------------------------------- #


# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                                     #
# ---------------------------------------------------------------------- # 

call UpgradeSchemaversion('1.71');

# ---------------------------------------------------------------------- #
# Drop foreign key constraints                                           #
# ---------------------------------------------------------------------- #

ALTER TABLE `service_type` DROP FOREIGN KEY `brand_TO_service_type`;

ALTER TABLE `service_type` DROP FOREIGN KEY `job_type_TO_service_type`;

ALTER TABLE `service_type` DROP FOREIGN KEY `user_TO_service_type`;

ALTER TABLE `job` DROP FOREIGN KEY `network_TO_job`;

ALTER TABLE `job` DROP FOREIGN KEY `branch_TO_job`;

ALTER TABLE `job` DROP FOREIGN KEY `client_TO_job`;

ALTER TABLE `job` DROP FOREIGN KEY `service_provider_TO_job`;

ALTER TABLE `job` DROP FOREIGN KEY `customer_TO_job`;

ALTER TABLE `job` DROP FOREIGN KEY `product_TO_job`;

ALTER TABLE `job` DROP FOREIGN KEY `service_type_TO_job`;

ALTER TABLE `job` DROP FOREIGN KEY `manufacturer_TO_job`;

ALTER TABLE `job` DROP FOREIGN KEY `job_type_TO_job`;

ALTER TABLE `job` DROP FOREIGN KEY `status_TO_job`;

ALTER TABLE `job` DROP FOREIGN KEY `model_TO_job`;

ALTER TABLE `job` DROP FOREIGN KEY `user_TO_job`;

ALTER TABLE `job` DROP FOREIGN KEY `user_TO_job_ModifiedUser`;

ALTER TABLE `part` DROP FOREIGN KEY `job_TO_part`;

ALTER TABLE `job_type` DROP FOREIGN KEY `brand_TO_job_type`;

ALTER TABLE `job_type` DROP FOREIGN KEY `user_TO_job_type`;

ALTER TABLE `service_type_alias` DROP FOREIGN KEY `service_type_TO_service_type_alias`;

ALTER TABLE `audit` DROP FOREIGN KEY `job_TO_audit`;

ALTER TABLE `appointment` DROP FOREIGN KEY `job_TO_appointment`;

ALTER TABLE `contact_history` DROP FOREIGN KEY `job_TO_contact_history`;

ALTER TABLE `status_history` DROP FOREIGN KEY `job_TO_status_history`;

ALTER TABLE `central_service_allocation` DROP FOREIGN KEY `job_type_TO_central_service_allocation`;

ALTER TABLE `central_service_allocation` DROP FOREIGN KEY `service_type_TO_central_service_allocation`;

ALTER TABLE `town_allocation` DROP FOREIGN KEY `job_type_TO_town_allocation`;

ALTER TABLE `town_allocation` DROP FOREIGN KEY `service_type_TO_town_allocation`;

ALTER TABLE `postcode_allocation` DROP FOREIGN KEY `job_type_TO_postcode_allocation`;

ALTER TABLE `postcode_allocation` DROP FOREIGN KEY `service_type_TO_postcode_allocation`;

# ---------------------------------------------------------------------- #
# Modify table "service_type"                                            #
# ---------------------------------------------------------------------- #

ALTER TABLE `service_type` ADD COLUMN `Priority` INTEGER;

ALTER TABLE `service_type` MODIFY `Priority` INTEGER AFTER `Code`;

# ---------------------------------------------------------------------- #
# Modify table "job"                                                     #
# ---------------------------------------------------------------------- #

ALTER TABLE `job` ADD COLUMN `StockCode` VARCHAR(64);

# ---------------------------------------------------------------------- #
# Modify table "part"                                                    #
# ---------------------------------------------------------------------- #

ALTER TABLE `part` ADD COLUMN `DueDate` DATE;

# ---------------------------------------------------------------------- #
# Modify table "job_type"                                                #
# ---------------------------------------------------------------------- #

ALTER TABLE `job_type` ADD COLUMN `Priority` INTEGER;

ALTER TABLE `job_type` MODIFY `Priority` INTEGER AFTER `TypeCode`;

# ---------------------------------------------------------------------- #
# Add foreign key constraints                                            #
# ---------------------------------------------------------------------- #

ALTER TABLE `service_type` ADD CONSTRAINT `brand_TO_service_type` 
    FOREIGN KEY (`BrandID`) REFERENCES `brand` (`BrandID`);

ALTER TABLE `service_type` ADD CONSTRAINT `job_type_TO_service_type` 
    FOREIGN KEY (`JobTypeID`) REFERENCES `job_type` (`JobTypeID`);

ALTER TABLE `service_type` ADD CONSTRAINT `user_TO_service_type` 
    FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `job` ADD CONSTRAINT `network_TO_job` 
    FOREIGN KEY (`NetworkID`) REFERENCES `network` (`NetworkID`);

ALTER TABLE `job` ADD CONSTRAINT `branch_TO_job` 
    FOREIGN KEY (`BranchID`) REFERENCES `branch` (`BranchID`);

ALTER TABLE `job` ADD CONSTRAINT `client_TO_job` 
    FOREIGN KEY (`ClientID`) REFERENCES `client` (`ClientID`);

ALTER TABLE `job` ADD CONSTRAINT `service_provider_TO_job` 
    FOREIGN KEY (`ServiceProviderID`) REFERENCES `service_provider` (`ServiceProviderID`);

ALTER TABLE `job` ADD CONSTRAINT `customer_TO_job` 
    FOREIGN KEY (`CustomerID`) REFERENCES `customer` (`CustomerID`);

ALTER TABLE `job` ADD CONSTRAINT `product_TO_job` 
    FOREIGN KEY (`ProductID`) REFERENCES `product` (`ProductID`);

ALTER TABLE `job` ADD CONSTRAINT `service_type_TO_job` 
    FOREIGN KEY (`ServiceTypeID`) REFERENCES `service_type` (`ServiceTypeID`);

ALTER TABLE `job` ADD CONSTRAINT `manufacturer_TO_job` 
    FOREIGN KEY (`ManufacturerID`) REFERENCES `manufacturer` (`ManufacturerID`);

ALTER TABLE `job` ADD CONSTRAINT `job_type_TO_job` 
    FOREIGN KEY (`JobTypeID`) REFERENCES `job_type` (`JobTypeID`);

ALTER TABLE `job` ADD CONSTRAINT `status_TO_job` 
    FOREIGN KEY (`StatusID`) REFERENCES `status` (`StatusID`);

ALTER TABLE `job` ADD CONSTRAINT `model_TO_job` 
    FOREIGN KEY (`ModelID`) REFERENCES `model` (`ModelID`);

ALTER TABLE `job` ADD CONSTRAINT `user_TO_job` 
    FOREIGN KEY (`BookedBy`) REFERENCES `user` (`UserID`);

ALTER TABLE `job` ADD CONSTRAINT `user_TO_job_ModifiedUser` 
    FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `part` ADD CONSTRAINT `job_TO_part` 
    FOREIGN KEY (`JobID`) REFERENCES `job` (`JobID`);

ALTER TABLE `job_type` ADD CONSTRAINT `brand_TO_job_type` 
    FOREIGN KEY (`BrandID`) REFERENCES `brand` (`BrandID`);

ALTER TABLE `job_type` ADD CONSTRAINT `user_TO_job_type` 
    FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `service_type_alias` ADD CONSTRAINT `service_type_TO_service_type_alias` 
    FOREIGN KEY (`ServiceTypeID`) REFERENCES `service_type` (`ServiceTypeID`);

ALTER TABLE `audit` ADD CONSTRAINT `job_TO_audit` 
    FOREIGN KEY (`JobID`) REFERENCES `job` (`JobID`);

ALTER TABLE `appointment` ADD CONSTRAINT `job_TO_appointment` 
    FOREIGN KEY (`JobID`) REFERENCES `job` (`JobID`);

ALTER TABLE `contact_history` ADD CONSTRAINT `job_TO_contact_history` 
    FOREIGN KEY (`JobID`) REFERENCES `job` (`JobID`);

ALTER TABLE `status_history` ADD CONSTRAINT `job_TO_status_history` 
    FOREIGN KEY (`JobID`) REFERENCES `job` (`JobID`);

ALTER TABLE `central_service_allocation` ADD CONSTRAINT `job_type_TO_central_service_allocation` 
    FOREIGN KEY (`JobTypeID`) REFERENCES `job_type` (`JobTypeID`);

ALTER TABLE `central_service_allocation` ADD CONSTRAINT `service_type_TO_central_service_allocation` 
    FOREIGN KEY (`ServiceTypeID`) REFERENCES `service_type` (`ServiceTypeID`);

ALTER TABLE `town_allocation` ADD CONSTRAINT `job_type_TO_town_allocation` 
    FOREIGN KEY (`JobTypeID`) REFERENCES `job_type` (`JobTypeID`);

ALTER TABLE `town_allocation` ADD CONSTRAINT `service_type_TO_town_allocation` 
    FOREIGN KEY (`ServiceTypeID`) REFERENCES `service_type` (`ServiceTypeID`);

ALTER TABLE `postcode_allocation` ADD CONSTRAINT `job_type_TO_postcode_allocation` 
    FOREIGN KEY (`JobTypeID`) REFERENCES `job_type` (`JobTypeID`);

ALTER TABLE `postcode_allocation` ADD CONSTRAINT `service_type_TO_postcode_allocation` 
    FOREIGN KEY (`ServiceTypeID`) REFERENCES `service_type` (`ServiceTypeID`);

# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #

insert into version (VersionNo) values ('1.72');
