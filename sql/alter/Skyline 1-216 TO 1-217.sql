# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 

call UpgradeSchemaVersion('1.216');

# ---------------------------------------------------------------------- #
# Modify Table appointment              	                             #
# ---------------------------------------------------------------------- #
ALTER TABLE appointment ADD COLUMN ForceAddReason VARCHAR(100) NULL AFTER GeoTagModiefiedByUser;


# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #
insert into version (VersionNo) values ('1.217');
