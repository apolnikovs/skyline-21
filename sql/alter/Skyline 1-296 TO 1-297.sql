# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 
call UpgradeSchemaVersion('1.296');

# ---------------------------------------------------------------------- #
# Vyk Changes 															 #
# ---------------------------------------------------------------------- # 
ALTER TABLE user_reports ADD COLUMN ScheduleStatus ENUM('on','off') NULL DEFAULT NULL AFTER Schedule;

# ---------------------------------------------------------------------- #
# Raju Changes 															 #
# ---------------------------------------------------------------------- # 
ALTER TABLE job_fault_code_lookup CHANGE ServiceProviderManufacturerID ManufacturerID INT( 11 ) NULL DEFAULT NULL; 
ALTER TABLE job_fault_code_lookup ADD isDeleted ENUM( 'Yes', 'No' ) NOT NULL DEFAULT 'No' AFTER ServiceProviderID; 
ALTER TABLE job_fault_code_lookup CHANGE RelatedPartFaultCode RelatedPartFaultCode ENUM( 'Yes', 'No' ) NULL DEFAULT NULL;


# ---------------------------------------------------------------------- #
# Mukesh Changes 														 #
# ---------------------------------------------------------------------- # 
CREATE TABLE IF NOT EXISTS gauge_preferences (
																GaugeID int(11) NOT NULL AUTO_INCREMENT, 
																GaDial0StatusID int(11) NOT NULL,
																GaDial0Yellow int(11) NOT NULL, 
																GaDial0Red int(11) NOT NULL, 
																GaDial1StatusID int(11) NOT NULL, 
																GaDial1Yellow int(11) NOT NULL, 
																GaDial1Red int(11) NOT NULL, 
																GaDial2StatusID int(11) NOT NULL, 
																GaDial2Yellow int(11) NOT NULL, 
																GaDial2Red int(11) NOT NULL, 
																GaDial3StatusID int(11) NOT NULL, 
																GaDial3Yellow int(11) NOT NULL, 
																GaDial3Red int(11) NOT NULL, 
																GaDial4StatusID int(11) NOT NULL, 
																GaDial4Yellow int(11) NOT NULL, 
																GaDial4Red int(11) NOT NULL, 
																GaDial5StatusID int(11) NOT NULL, 
																GaDial5Yellow int(11) NOT NULL, 
																GaDial5Red int(11) NOT NULL, 
																UserID int(11) NOT NULL, 
																CreatedDate datetime NOT NULL, 
																EndDate datetime NOT NULL, 
																PRIMARY KEY (GaugeID), 
																UNIQUE KEY Unique_gauge_user (GaDial0StatusID,GaDial1StatusID,GaDial2StatusID,GaDial3StatusID,GaDial4StatusID,UserID), 
																KEY GaDial1StatusID (GaDial1StatusID), 
																KEY GaDial2StatusID (GaDial2StatusID), 
																KEY GaDial3StatusID (GaDial3StatusID), 
																KEY GaDial4StatusID (GaDial4StatusID), 
																KEY UserID (UserID) 
															) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

ALTER TABLE gauge_preferences ADD CONSTRAINT gauge_preferences_ibfk_1 FOREIGN KEY (GaDial0StatusID) REFERENCES completion_status (CompletionStatusID),
							  ADD CONSTRAINT gauge_preferences_ibfk_2 FOREIGN KEY (GaDial1StatusID) REFERENCES completion_status (CompletionStatusID), 
							  ADD CONSTRAINT gauge_preferences_ibfk_3 FOREIGN KEY (GaDial2StatusID) REFERENCES completion_status (CompletionStatusID), 
							  ADD CONSTRAINT gauge_preferences_ibfk_4 FOREIGN KEY (GaDial3StatusID) REFERENCES completion_status (CompletionStatusID), 
							  ADD CONSTRAINT gauge_preferences_ibfk_5 FOREIGN KEY (GaDial4StatusID) REFERENCES completion_status (CompletionStatusID), 
							  ADD CONSTRAINT gauge_preferences_ibfk_6 FOREIGN KEY (UserID) REFERENCES user (UserID);


# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #
insert into version (VersionNo) values ('1.297');
