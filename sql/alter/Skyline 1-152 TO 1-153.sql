# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 

call UpgradeSchemaVersion('1.152');


# ---------------------------------------------------------------------- #
# Modify table "service_provider"                                                     #
# ---------------------------------------------------------------------- #
ALTER TABLE `service_provider` ADD COLUMN `EngineerDefaultStartTime` TIME NULL AFTER `SendASCReport`;
ALTER TABLE `service_provider` ADD COLUMN `EngineerDefaultEndTime` TIME NULL AFTER `EngineerDefaultStartTime`;



# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #

insert into version (VersionNo) values ('1.153');


