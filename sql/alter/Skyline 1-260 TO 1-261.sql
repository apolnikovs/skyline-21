# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 
call UpgradeSchemaVersion('1.260');

# ---------------------------------------------------------------------- #
# Modify Table user_reports                                              #
# ---------------------------------------------------------------------- # 
ALTER TABLE user_reports ALTER ReportName DROP DEFAULT; 
ALTER TABLE user_reports CHANGE COLUMN ReportName Name VARCHAR(250) NOT NULL AFTER UserID, 
						 CHANGE COLUMN ReportDescription Description TEXT NULL DEFAULT NULL AFTER Name, 
						 CHANGE COLUMN ReportStructure Structure TEXT NOT NULL AFTER Description, 
						 ADD COLUMN ModifiedUserID INT(11) NULL DEFAULT NULL AFTER Structure, 
						 ADD COLUMN ModifiedDate TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP AFTER ModifiedUserID, 
						 ADD CONSTRAINT user_TO_user_reports1 FOREIGN KEY (ModifiedUserID) REFERENCES user (UserID) ON UPDATE NO ACTION ON DELETE NO ACTION;

# ---------------------------------------------------------------------- #
# Modify Table service_provider                                          #
# ---------------------------------------------------------------------- # 
ALTER TABLE service_provider ADD COLUMN AutoReorderStock ENUM('Yes','No') NULL DEFAULT 'No' AFTER BookingCapacityLimit;


# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #
insert into version (VersionNo) values ('1.261');
