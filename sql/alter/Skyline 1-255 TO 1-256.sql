# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 
call UpgradeSchemaVersion('1.255');

# ---------------------------------------------------------------------- #
# Modify Table viamente_end_day                                          #
# ---------------------------------------------------------------------- # 
ALTER TABLE `viamente_end_day` ADD COLUMN `EndDayType` ENUM('Both','Brown','White') NOT NULL DEFAULT 'Both' AFTER `ModifiedDate`;


# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #
insert into version (VersionNo) values ('1.256');
