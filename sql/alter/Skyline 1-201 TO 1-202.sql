# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 

call UpgradeSchemaVersion('1.201');

# ---------------------------------------------------------------------- #
# Modify table "appointment"                                             #
# ---------------------------------------------------------------------- #
ALTER TABLE `appointment` CHANGE COLUMN `CompletedBy` `CompletedBy` ENUM('Remote Engineer','Manual','Out Card Left') NULL DEFAULT NULL AFTER `AppointmentEndTime2`;


# ---------------------------------------------------------------------- #
# Add table "sp_engineer_day_location"                                   #
# ---------------------------------------------------------------------- #
CREATE TABLE `sp_engineer_day_location` (
	`SpEngineerDayLocation` INT(10) NOT NULL AUTO_INCREMENT,
	`ServiceProviderEngineerID` INT(10) NOT NULL DEFAULT '0',
	`Langitude` DECIMAL(17,4) NOT NULL DEFAULT '0.0000',
	`Longitude` DECIMAL(17,4) NOT NULL DEFAULT '0.0000',
	`DateModified` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`LunchTaken` ENUM('Yes','No') NOT NULL DEFAULT 'No',
	PRIMARY KEY (`SpEngineerDayLocation`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB;



# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #
insert into version (VersionNo) values ('1.202');
