# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 

call UpgradeSchemaVersion('1.192');

# ---------------------------------------------------------------------- #
# Add table "appointment"                                                #
# ---------------------------------------------------------------------- #
ALTER TABLE `preferential_clients_manufacturers` CHANGE COLUMN `PreferentialType` `PreferentialType` ENUM('Manufacturer','Brand','ServiceProvider') NOT NULL AFTER `PageName`;


# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #

insert into version (VersionNo) values ('1.193');
