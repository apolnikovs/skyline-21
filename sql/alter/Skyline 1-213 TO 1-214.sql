# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 

call UpgradeSchemaVersion('1.213');

# ---------------------------------------------------------------------- #
# Modify Table service_provider         	                             #
# ---------------------------------------------------------------------- #
ALTER TABLE service_provider ADD COLUMN ViamenteKey2 VARCHAR(50) NULL DEFAULT NULL, 
ADD COLUMN ViamenteKey2MaxEng INT NULL DEFAULT '0' AFTER ViamenteKey2, 
ADD COLUMN ViamenteKey2MaxWayPoints INT NULL DEFAULT '0' AFTER ViamenteKey2MaxEng;

# ---------------------------------------------------------------------- #
# Modify Table service_provider_engineer	                             #
# ---------------------------------------------------------------------- #
ALTER TABLE service_provider_engineer CHANGE COLUMN AppsBeforeOptimise AppsBeforeOptimise TINYINT(4) NOT NULL DEFAULT '0' AFTER ViamenteStartType, 
ADD COLUMN PrimarySkill ENUM('Brown Goods','White Goods') NULL DEFAULT NULL AFTER AppsBeforeOptimise;


				
# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #
insert into version (VersionNo) values ('1.214');
