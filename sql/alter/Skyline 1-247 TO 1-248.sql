# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 
call UpgradeSchemaVersion('1.247');

# ---------------------------------------------------------------------- #
# Add one_touch_audit Table                                              #
# ---------------------------------------------------------------------- # 
CREATE TABLE `one_touch_audit` (
	`OneTouchAuditID` INT(11) NOT NULL AUTO_INCREMENT,
	`Type` ENUM('getAvailability','putReservation','putCancelJob') NULL DEFAULT NULL,
	`ActionDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`Username` VARCHAR(20) NULL,
	`IPAddress` VARCHAR(20) NULL,
	`ServiceProviderID` INT(11) NULL,
	`ServiceOrder` VARCHAR(20) NULL,
	`ModelID` INT(11) NULL,
	`ModelName` VARCHAR(24) NULL,
	`ProductGroup` VARCHAR(10) NULL,
	`Postcode` VARCHAR(10) NULL,
	`Country` VARCHAR(10) NULL,
	`Region` VARCHAR(20) NULL,
	`City` VARCHAR(40) NULL,
	`Address1` VARCHAR(60) NULL,
	`Address2` VARCHAR(40) NULL,
	`Address3` VARCHAR(40) NULL,
	`Day1AMAvailability` ENUM('Yes','No') NULL,
	`Day1PMAvailability` ENUM('Yes','No') NULL,
	`Day2AMAvailability` ENUM('Yes','No') NULL,
	`Day2PMAvailability` ENUM('Yes','No') NULL,
	`Day3AMAvailability` ENUM('Yes','No') NULL,
	`Day3PMAvailability` ENUM('Yes','No') NULL,
	`Day4AMAvailability` ENUM('Yes','No') NULL,
	`Day4PMAvailability` ENUM('Yes','No') NULL,
	`Day5AMAvailability` ENUM('Yes','No') NULL,
	`Day5PMAvailability` ENUM('Yes','No') NULL,
	`Day6AMAvailability` ENUM('Yes','No') NULL,
	`Day6PMAvailability` ENUM('Yes','No') NULL,
	`Day7AMAvailability` ENUM('Yes','No') NULL,
	`Day7PMAvailability` ENUM('Yes','No') NULL,
	PRIMARY KEY (`OneTouchAuditID`),
	CONSTRAINT `model_TO_one_touch_audit` FOREIGN KEY (`ModelID`) REFERENCES `model` (`ModelID`),
	CONSTRAINT `service_provider_TO_one_touch_audit` FOREIGN KEY (`ServiceProviderID`) REFERENCES `service_provider` (`ServiceProviderID`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB;


# ---------------------------------------------------------------------- #
# Part Stock Changes For Andris                                          #
# ---------------------------------------------------------------------- # 
SET foreign_key_checks = 0;

ALTER TABLE sp_part_stock_item ADD COLUMN CreatedDate TIMESTAMP NULL AFTER ModifiedDate, ADD COLUMN CreatedUserID INT NULL AFTER CreatedDate;

ALTER TABLE sp_part_status ALTER Description DROP DEFAULT; ALTER TABLE sp_part_status CHANGE COLUMN Description Description VARCHAR(50) NOT NULL AFTER SPPartStatusID;

ALTER TABLE sp_part_stock_item ALTER ModifiedByUser DROP DEFAULT; ALTER TABLE sp_part_stock_item CHANGE COLUMN ModifiedByUser ModifiedUserID INT(10) NOT NULL AFTER PurchaseCost;

CREATE TABLE sp_part_order ( SPPartOrderID INT(11) NOT NULL AUTO_INCREMENT, ServiceProviderSupplierID INT(11) NULL DEFAULT NULL, OrderNo VARCHAR(50) NULL DEFAULT NULL, OrderedDate DATETIME NULL DEFAULT NULL, ReceviedDate DATETIME NULL DEFAULT NULL, Received ENUM('Y','N') NULL DEFAULT NULL, ModifiedDate DATETIME NULL DEFAULT NULL, ModifiedUserID INT(11) NULL DEFAULT NULL, ReceivedByUserID INT(11) NULL DEFAULT NULL, CreatedDate TIMESTAMP NULL DEFAULT NULL, CreatedUserID INT(11) NULL DEFAULT NULL, ServiceProviderID INT(10) NULL DEFAULT NULL, CancelledDate TIMESTAMP NULL DEFAULT NULL, Status ENUM('Active','In-active') NOT NULL DEFAULT 'Active', PRIMARY KEY (SPPartOrderID), INDEX OrderNo (OrderNo), INDEX ServiceProviderSupplierID (ServiceProviderSupplierID) ) COLLATE='latin1_swedish_ci' ENGINE=InnoDB AUTO_INCREMENT=1;

REPLACE INTO sp_part_status (SPPartStatusID, Description, Available, InStock) VALUES (1, '00 An initial stock import', 'Y', 'Y'), (2, '01 Part Order being received', 'Y', 'Y'), (3, '03 Part Fitted', 'N', 'N'), (4, '04 Part Sold', 'N', 'N');

SET foreign_key_checks = 1;
# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #
insert into version (VersionNo) values ('1.248');
