# ---------------------------------------------------------------------- #
# Script generated with: DeZign for Databases V7.2.0                     #
# Target DBMS:           MySQL 5                                         #
# Project file:          SkyLine.dez                                     #
# Project name:          SkyLine                                         #
# Author:                Brian Etherington                               #
# Script type:           Alter database script                           #
# Created on:            2012-10-16 16:26                                #
# ---------------------------------------------------------------------- #


# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                                     #
# ---------------------------------------------------------------------- # 

call UpgradeSchemaVersion('1.97');

# ---------------------------------------------------------------------- #
# Add table "primary_fields"                                             #
# ---------------------------------------------------------------------- #

CREATE TABLE `primary_fields` (
    `primaryFieldID` INTEGER NOT NULL AUTO_INCREMENT,
    `primaryFieldName` VARCHAR(250) NOT NULL,
    CONSTRAINT `primary_fieldsID` PRIMARY KEY (`primaryFieldID`)
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

# ---------------------------------------------------------------------- #
# Add table "alternative_fields"                                         #
# ---------------------------------------------------------------------- #

CREATE TABLE `alternative_fields` (
    `alternativeFieldID` INTEGER NOT NULL AUTO_INCREMENT,
    `primaryFieldID` INTEGER NOT NULL,
    `alternativeFieldName` VARCHAR(250) NOT NULL,
    `status` ENUM('Active','In-active') NOT NULL DEFAULT 'Active',
    `brandID` INTEGER NOT NULL,
    CONSTRAINT `alternative_fieldsID` PRIMARY KEY (`alternativeFieldID`)
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #

insert into version (VersionNo) values ('1.98');
