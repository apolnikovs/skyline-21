# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 
call UpgradeSchemaVersion('1.302');

# ---------------------------------------------------------------------- #
# Mukesh Mishra Changes 												 #
# ---------------------------------------------------------------------- # 
ALTER TABLE gauge_preferences ADD GaDial0Swap BOOLEAN NOT NULL DEFAULT FALSE AFTER GaDial5Red, 
							  ADD GaDial1Swap BOOLEAN NOT NULL DEFAULT FALSE AFTER GaDial0Swap, 
							  ADD GaDial2Swap BOOLEAN NOT NULL DEFAULT FALSE AFTER GaDial1Swap, 
							  ADD GaDial3Swap BOOLEAN NOT NULL DEFAULT FALSE AFTER GaDial2Swap, 
							  ADD GaDial4Swap BOOLEAN NOT NULL DEFAULT FALSE AFTER GaDial3Swap, 
							  ADD GaDial5Swap BOOLEAN NOT NULL DEFAULT FALSE AFTER GaDial4Swap;

# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #
insert into version (VersionNo) values ('1.303');



