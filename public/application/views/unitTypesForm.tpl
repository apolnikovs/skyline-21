{if $accessErrorFlag == true} 
    
    <div id="accessDeniedMsg" class="SystemAdminFormPanel">   
        <form id="accessDeniedMsgForm" name="accessDeniedMsgForm" method="post" action="#" class="inline">
	    <fieldset>
		<legend title="">{$page['Text']['error_page_legend']|escape:'html'}</legend>
		<p>
		    <label class="formCommonError">{$accessDeniedMsg|escape:'html'}</label>
		    <br/><br/>
		</p>

		<p>
		    <span class="bottomButtons">
			<input type="submit" name="cancel_btn" class="textSubmitButton rightBtn" id="cancel_btn" onclick="return false;" value="{$page['Buttons']['ok']|escape:'html'}" />
		    </span>
		</p>
	    </fieldset>   
	</form>            
    </div>    
    
{else}  
    
    <script type="text/javascript" >
              
        
	$(document).ready(function() {
            $("#RepairSkillID").combobox();    
            /* =======================================================
            *
            * Initialise input auto-hint functions...
            *
            * ======================================================= */

            $('.auto-hint').focus(function() {
		$this = $(this);
		if($this.val() == $this.attr('title')) {
		    $this.val('').removeClass('auto-hint');
		    if($this.hasClass('auto-pwd')) {
			$this.prop('type','password');
		    }
		}
	    }).blur(function() {
		$this = $(this);
		if($this.val() == '' && $this.attr('title') != '')  {
		    $this.val($this.attr('title')).addClass('auto-hint');
		    if($this.hasClass('auto-pwd')) {
			$this.prop('type','text');
		    }
		}         
	    }).each(function() {
		$this = $(this);
		if($this.attr('title') == '') { return; }
		if($this.val() == '') { 
		    if($this.attr('type') == 'password') {
			$this.addClass('auto-pwd').prop('type','text');
		    }
		    $this.val($this.attr('title')); 
		} else { 
		    $this.removeClass('auto-hint'); 
		}
		$this.attr('autocomplete','off');
	    }); 
            
            
            $("#AccessoryID").multiselect( {  noneSelectedText: "{$page['Text']['select_default_option']|escape:'html'}", minWidth: 200, height: "auto", show:['slide', 500]  } );
            
            $(".ui-multiselect").css("width", "310px");
            
            $('.fieldLabel').css("width", "250px");
            
            
	});       
        
    </script>
   
     {* Updated by Praveen Kumar N *}
     
     <script type="text/javascript" >
              
     $(document).ready(function(){
         $(":radio:eq(0)").click(function(){
             $("#Toggle_IME_Length").show(500);
          });

          $(":radio:eq(1)").click(function(){
             $("#Toggle_IME_Length").hide(500);
          });

    });
  
    </script>
     
    
    <div id="UnitTypesFormPanel" class="SystemAdminFormPanel">
    
	<form id="UnitTypesForm" name="UnitTypesForm" method="post" action="#" class="inline">
       
	    <fieldset>
		
		<legend title="">{$form_legend|escape:'html'}</legend>
		<p><label id="suggestText"></label></p>
		<p>
		    <label></label>
		    <span class="topText">
			{$page['Text']['top_info_text1']|escape:'html'} <span>*</span> {$page['Text']['top_info_text2']|escape:'html'}
		    </span>
		</p>
		<p>
		    <label class="fieldLabel" for="UnitTypeName">{$page['Labels']['unit_type']|escape:'html'}:<sup>*</sup></label>
		    &nbsp;&nbsp; 
		    <input type="text" class="text" name="UnitTypeName" value="{$datarow.UnitTypeName|escape:'html'}" id="UnitTypeName" />
		</p>
                         
		{*
		<p>
		   <label class="fieldLabel" for="JobTypeID" >{$page['Labels']['job_type']|escape:'html'}:</label>

		    &nbsp;&nbsp;  
		    <select name="JobTypeID" id="JobTypeID" class="text" >
		       <option value="" {if $datarow.JobTypeID eq ''}selected="selected"{/if}>{$page['Text']['select_default_option']|escape:'html'}</option>

		       {foreach $jobTypes as $jobType}

			   <option value="{$jobType.JobTypeID}" {if $datarow.JobTypeID eq $jobType.JobTypeID}selected="selected"{/if}>{$jobType.Type|escape:'html'}</option>

		       {/foreach}

		   </select>
		 </p>*}
                         
                         
		<p>
		    <label class="fieldLabel" for="RepairSkillID">{$page['Labels']['repair_skill']|escape:'html'}:</label>
		    &nbsp;&nbsp;  
		    <select name="RepairSkillID" id="RepairSkillID" class="text">
			<option value="" {if $datarow.RepairSkillID eq ''}selected="selected"{/if}>{$page['Text']['select_default_option']|escape:'html'}</option>
			{foreach $repairSkills as $repairSkill}
			    <option value="{$repairSkill.RepairSkillID}" {if $datarow.RepairSkillID == $repairSkill.RepairSkillID}selected="selected"{/if}>{$repairSkill.RepairSkillName|escape:'html'}</option>
			{/foreach}
		    </select>
		</p>
                
                
                
                <p>
		    <label class="fieldLabel" for="AccessoryID" >{$page['Labels']['enable_accessories']|escape:'html'}:</label>
		    &nbsp;&nbsp;  
		    <select name="AccessoryID[]" id="AccessoryID" class="text" multiple="multiple" >
			{foreach $AccessoriesList as $Accessory}
			    <option value="{$Accessory.AccessoryID}"   {if in_array($Accessory.AccessoryID, $datarow.AccessoryID)}selected="selected"{/if} >{$Accessory.AccessoryName|escape:'html'}</option>
			{/foreach}
		    </select>
		</p>
                
                
                
                <p>
		    <label class="fieldLabel" for="DOPWarrantyPeriod">{$page['Labels']['dop_warranty_period']|escape:'html'}:</label>
		    &nbsp;&nbsp; 
		    <input type="text" class="text" name="DOPWarrantyPeriod" value="{$datarow.DOPWarrantyPeriod|escape:'html'}" id="DOPWarrantyPeriod" />
                    {$page['Text']['months']|escape:'html'}
		</p>
                
                
                <p>
		    <label class="fieldLabel" for="ProductionDateWarrantyPeriod">{$page['Labels']['production_date_warranty_period']|escape:'html'}:</label>
		    &nbsp;&nbsp; 
		    <input type="text" class="text" name="ProductionDateWarrantyPeriod" value="{$datarow.ProductionDateWarrantyPeriod|escape:'html'}" id="ProductionDateWarrantyPeriod" />
                    {$page['Text']['months']|escape:'html'}
		</p>
                
                
                
                          
		<p>
		    <label class="fieldLabel" for="ImeiRequired">{$page['Labels']['imei_required']|escape:'html'}:<sup>*</sup></label>
		    &nbsp;&nbsp; 
		    <input type="radio" name="ImeiRequired" value="Yes" {if $datarow.ImeiRequired == "Yes"}checked="checked"{/if} />
		    <span class="text">Yes</span>
		    <input type="radio" name="ImeiRequired" value="No" {if $datarow.ImeiRequired == "No"}checked="checked"{/if} />
		    <span class="text">No</span>
		</p>
		
		{* Updated By Praveen Kumar N [04/07/2013]  :: START *}
		
		
		<div id="Toggle_IME_Length"> 
		    
		 <p>
		    <label class="fieldLabel" for="IMEILengthBetween">IMEI Length Between:<sup>*</sup></label>
		    &nbsp;&nbsp; 
		    <input type="text" name="IMEILengthFrom" id="IMEILengthFrom" style="width:30px; text-align: center;" maxlength="2" value="{$datarow.IMEILengthFrom}" />
		    <span class="text" style="padding-left: 6px;">And</span>
		    <input type="text" name="IMEILengthTo" id="IMEILengthTo" style="width:30px; text-align: center;" maxlength="2" value="{$datarow.IMEILengthTo}" />
		    
		</p>
		
		</div>
		
		
		{* Updated By Praveen Kumar N [04/07/2013]  :: END *}
		
		<p>
		    <label class="fieldLabel" for="Status">{$page['Labels']['status']|escape:'html'}:<sup>*</sup></label>
		    &nbsp;&nbsp; 
		    {foreach $statuses as $status}
			<input type="radio" name="Status" value="{$status.Code}" {if $datarow.Status == $status.Code}checked="checked"{/if} />
			<span class="text" style="width: 60px;">{$status.Name|escape:'html'}</span> 
		    {/foreach}    
		</p>

		<p>
		    <span class= "bottomButtons">
			<input type="hidden" name="UnitTypeID" value="{$datarow.UnitTypeID|escape:'html'}" />
			{if $datarow.UnitTypeID != '' && $datarow.UnitTypeID != '0'}
			    <input type="submit" name="update_save_btn" class="textSubmitButton centerBtn" id="update_save_btn" value="{$page['Buttons']['save']|escape:'html'}" />
			{else}
			    <input type="submit" name="insert_save_btn" class="textSubmitButton centerBtn" id="insert_save_btn" value="{$page['Buttons']['save']|escape:'html'}" />
			{/if}
			<br/>
			<input type="submit" name="cancel_btn" class="textSubmitButton rightBtn" id="cancel_btn" onclick="return false;" value="{$page['Buttons']['cancel']|escape:'html'}" />
		    </span>
		</p>
                           
	    </fieldset>    
                        
	</form>        
       
    </div>
                 
{/if}  
                                    
                                    
{* This block of code is for to display message after data updation *} 

                                    
<div id="dataUpdatedMsg" class="SystemAdminFormPanel" style="display:none;">   
    
    <form id="dataUpdatedMsgForm" name="dataUpdatedMsgForm" method="post" action="#" class="inline">
	<fieldset>
	    <legend title="">{$form_legend|escape:'html'}</legend>
	    <p>
		<b>{$page['Text']['data_updated_msg']|escape:'html'}</b>
		<br/><br/>
	    </p>
	    <p>
		<span class= "bottomButtons">
		    <input type="submit" name="cancel_btn" class="textSubmitButton rightBtn" id="cancel_btn" onclick="return false;" value="{$page['Buttons']['ok']|escape:'html'}" />
		</span>
	    </p>
	</fieldset>   
    </form>            
    
</div>    
    
 
{* This block of code is for to display message after data insertion *}                      
                        
<div id="dataInsertedMsg" class="SystemAdminFormPanel" style="display:none;">   
    
    <form id="dataInsertedMsgForm" name="dataInsertedMsgForm" method="post" action="#" class="inline">
	<fieldset>
	    <legend title="">{$form_legend|escape:'html'}</legend>
	    <p>
		<b>{$page['Text']['data_inserted_msg']|escape:'html'}</b>
		<br/><br/>
	    </p>
	    <p>
		<span class="bottomButtons">
		    <input type="submit" name="cancel_btn" class="textSubmitButton rightBtn" id="cancel_btn" onclick="return false;" value="{$page['Buttons']['ok']|escape:'html'}" />
		</span>
	    </p>
	</fieldset>   
    </form>            
    
</div>                            
                        
