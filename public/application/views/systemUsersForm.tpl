{if $accessErrorFlag eq true} 
    
    <div id="accessDeniedMsg" class="SystemAdminFormPanel"  >   
    
        <form id="accessDeniedMsgForm" name="accessDeniedMsgForm" method="post"  action="#" class="inline">
                <fieldset>
                    <legend title="" > {$page['Text']['error_page_legend']|escape:'html'} </legend>
                    <p>

                        <label class="formCommonError" >{$accessDeniedMsg|escape:'html'}</label><br><br>

                    </p>

                    <p>

                      <span class= "bottomButtons" >
                        <input type="submit" name="cancel_btn" class="textSubmitButton rightBtn" 
                               id="cancel_btn" onclick="return false;"  value="{$page['Buttons']['ok']|escape:'html'}" />

                        </span>

                    </p>


                </fieldset>   
        </form>            


    </div>    
    
{else}  
    
    <script type="text/javascript" >

    function addFields(selected) {	
        var selected_split = selected.split(","); 	

        $("#BuildingNameNumber").val(selected_split[0]).blur();	
        $("#Street").val(selected_split[1]).blur();
        $("#LocalArea").val(selected_split[2]).blur();
        $("#TownCity").val(selected_split[3]).blur();
        $("#selectOutput").slideUp("slow"); 

    }
    loadDefaultBrand = function(){
        if($('#CompanyID').val() != '') {
            $.ajax({
                type:   "POST",
                url:    "{$_subdomain}/Data/companyBrands",
                data:   "UserID={$datarow.UserID}&UserType="+$('#UserType').val()+"&CompanyID="+$('#CompanyID').val(),
                dataType: "html",
                async: false,
                success:    function(data, textStatus){
                    var obj = jQuery.parseJSON( data );

                    $('#DefaultBrandID').html( $('#DefaultBrandID option').first() );
                    $('#DefaultBrandID').removeAttr("disabled");
                    if($('#UserType').val()=="ServiceProvider"){
                      $('#DefaultBrandID').find('option').remove().end();
                    }
                    $.each(obj.brand, function(index, item) {
                         var selected = ($('#DefaultBrandID_temp').val() == item.BrandID && $('#UserType_temp').val() == $('#UserType').val() ? 'selected="selected"' : '');
                         $('#DefaultBrandID').append('<option value="'+item.BrandID+'" ' + selected + '>'+item.BrandName+'</option>');
                    });
                }
            });
        }
    }    
    $(document).ready(function() {
        $("#CustomerTitleID").combobox();
        $("#SecurityQuestionID").combobox();
        $("#UserType").combobox({
            change: function() {
                branchSearchScopeDisplay();
                loadCompanyBrand(); 
                $("#CompanyIDLabel").html($("#UserType option:selected").html()+": <sup>*</sup>");
                $('#CompanyID').focus();
            }
        });
        $("#CompanyID").combobox({
            change: function() {
                if($('#CompanyID').val() != ''){
                    loadDefaultBrand();
                    $('#BrandID').focus(); 
                }else{
                    $('#CompanyID').focus();
                    $('#DefaultBrandID').val('');
                    $('#DefaultBrandID').attr({ disabled : 'disabled' });
                }
            }
        });
        $("#NetworkID").combobox({
            change: function() {
                if($('#NetworkID').val() != ''){
                    $.ajax({
                        type:   "GET",
                        url:    "{$_subdomain}/Data/getNetworkClients",
                        data:   "networkID="+$('#NetworkID').val(),
                        dataType: "json",
                        async: false,
                        success:    function(data){
                            if($('#UserType').val()=="Client"){
                                $('#DefaultBrandID').find('option').remove().end()
                                $('#CompanyID').find('option').remove().end()
                            }
                            $.each(data, function(i, item) {
                                if($('#UserType').val()=="Client"){
                                    $('#CompanyID').append('<option value="'+item.ClientID+'">'+item.ClientName+'</option>');
                                    $('#CompanyID').combobox();
                                } else {
                                    $('#ClientID').append('<option value="'+item.ClientID+'">'+item.ClientName+'</option>');
                                    $('#ClientID').combobox();
                                }
                            });
                        },//end success function 
                        error : function(){
                            console.error('error');
                        }
                    });
                    if($('#UserType').val()=="Client")
                        $('#CompanyID').focus();
                    else
                        $('#ClientID').focus();
                }else{
                    $('#NetworkID').focus();
                    $('#ClientID').val('');
                }
            }
        });
        $("#ClientID").combobox({
            change: function() {
                if($('#ClientID').val() != ''){
                    $.ajax({
                        type:   "GET",
                        url:    "{$_subdomain}/Data/getClientBranches",
                        data:   "clientID="+$('#ClientID').val(),
                        dataType: "json",
                        async: false,
                        success:    function(data){
                            if($('#UserType').val()=="Branch"){
                                $('#DefaultBrandID').find('option').remove().end()
                                $('#CompanyID').find('option').remove().end()
                            }
                            $.each(data, function(i, item) {
                                $('#CompanyID').append('<option value="'+item.BranchID+'">'+item.BranchName+'</option>');
                                $('#CompanyID').combobox();
                            });
                        },//end success function 
                        error : function(){
                            console.error('error');
                        }
                    });
                    $('#ClientID').focus(); 
                }else{
                    $('#NetworkID').focus();
                    $('#ClientID').val('');
                }
            }
        });
        $("#DefaultBrandID").combobox();
        $("#BranchJobSearchScope").combobox();
        $("#accessRightDropdown").combobox({
            change: function() {
                addAccessRight();
            }
        });
            if($("#UserType option:selected").val())
            {    
                $("#CompanyIDLabel").html($("#UserType option:selected").html());
            }    
             
             function autoHint() {
                 $('.auto-hint').each(function() {
                                    $this = $(this);
                                    if ($this.val() == $this.attr('title')) {
                                        $this.val('').removeClass('auto-hint').addClass('auto-hint-hide');
                                        if ($this.hasClass('auto-pwd')) {
                                            $this.prop('type','password');
                                        }
                                    }
                        } ); 
             }
             
             
             
            $(document).on('click', '#insert_save_btn, #update_save_btn', function(){ autoHint(); });
                
            insertAccessRightHTML = function(RoleID, Name){
                                $('#accessRight').append('<li value="'+RoleID+'" style="clear:both">'+
                                '<span class="left" id="Role_ID_'+RoleID+'"  >'+Name+'</span>'+
                                '<span class="left"><img class="close" src="{$_subdomain}/css/Skins/{$_theme}/images/close.png" /></span> </li>');             
            }    
            
            loadCompanyBrand = function(){
                if($('#UserType').val() != ''){
                    $.ajax({
                        type:   "POST",
                        url:    "{$_subdomain}/Data/companyBrands",
                        data:   "UserID={$datarow.UserID}&UserType="+$('#UserType').val(),
                        dataType: "html",
                        async: false,
                        success:    function(data, textStatus){
                            var obj = jQuery.parseJSON( data );

//                            $('#DefaultBrandID').html( $('#DefaultBrandID option').first() );
//                            $('#DefaultBrandID').removeAttr("disabled");
//                            $.each(obj.brand, function(index, item) {
//                                 var selected = ($('#DefaultBrandID_temp').val() == item.BrandID ? 'selected="selected"' : '');
//
//                                 $('#DefaultBrandID').append('<option value="'+item.BrandID+'" ' + selected + '>'+item.BrandName+'</option>');
//                            });
                            
                            $('#CompanyID').html( $('#CompanyID option').first() );
                            $('#CompanyID').removeAttr("disabled");
                            $.each(obj.company, function(index, item) {
                                var selected = ($('#CompanyID_temp').val() == item.ID && $('#UserType_temp').val() == $('#UserType').val() ? 'selected="selected"' : '');
                                
                                $('#CompanyID').append('<option value="'+item.ID+'" ' + selected + '>'+item.CompanyName+'</option>');
                            });
                            if($('#CompanyID').val() == ''){
                                $('#DefaultBrandID').val('');
                                $('#DefaultBrandID').attr({ disabled : 'disabled' })
                            }
                            
                            //accessRightDropdown
                            $('#accessRightDropdown').html( $('#accessRightDropdown option').first() );
                            $('#accessRightDropdown').removeAttr("disabled");
                             
                            $('#accessRight').html(''); 
                            if(obj.access_right.assigned){
                        
                                $.each(obj.access_right.assigned, function(index, item) {

                                    insertAccessRightHTML(item.RoleID, item.Name);
                                });

                                $.each(obj.access_right.nonassigned, function(index, item) {

                                    $('#accessRightDropdown').append('<option value="'+item.RoleID+'">'+item.Name+'</option>');       
                                });   
                            }else{
                                $.each(obj.access_right, function(index, item) {

                                    $('#accessRightDropdown').append('<option value="'+item.RoleID+'">'+item.Name+'</option>');    
                                });                              
                            }                                
                            
                        },//end success function 
                        error : function(){
                            console.error('error loadCompanyBrand()');
                        }
                    });
                    setValue("#CompanyID");
                    setValue("#DefaultBrandID");
                }else{
                    $('#CompanyID').val(''); 
                    $('#DefaultBrandID').val(''); 
                    $('#accessRightDropdown').val('');                
                    $('#CompanyID').attr({ disabled:'disabled' });  
                    $('#DefaultBrandID').attr({ disabled:'disabled' });  
                    $('#accessRightDropdown').attr({ disabled:'disabled' });  
                }
            }
            loadCompanyBrand();
            //loadDefaultBrand();
            
            /*
             * Deal with brand_job_search_scope box
             */
          
           branchSearchScopeDisplay = function(){  
                if($('#UserType :selected').text() === 'Branch'){
                    $('#branch_job_network_fields').show();
                    $('#branch_job_client_fields').show();
                    $('#branch_job_search_scope_fields').show();
                } else if($('#UserType :selected').text() === 'Client'){
                    $('#branch_job_network_fields').show();
                    $('#branch_job_client_fields').hide();
                    $('#branch_job_search_scope_fields').hide();
                    
                }else {
                    $('#branch_job_network_fields').hide();
                    $('#branch_job_client_fields').hide();
                    $('#branch_job_search_scope_fields').hide();
                }
           }
           branchSearchScopeDisplay();
           
            
            /*$(document).on('change', '#UserType', function(){
                branchSearchScopeDisplay();
                loadCompanyBrand(); 
                
                
                $("#CompanyIDLabel").html($("#UserType option:selected").html()+": <sup>*</sup>");
                
                
                $('#CompanyID').focus();
            });*/
            /*$(document).on('change', '#CompanyID', function(){ 
                if($('#CompanyID').val() != ''){
                    //loadDefaultBrand();
                    $('#BrandID').focus(); 
                }else{
                    $('#CompanyID').focus();
                    $('#DefaultBrandID').val('');
                    $('#DefaultBrandID').attr({ disabled : 'disabled' });
                }
            });*/
                            
             $(document).on('click', '#accessRight li',
                function(){
                    console.log( $(this).val() );
                    
                    //alert($(this).html());
                    
                   // alert();
                  //  alert($("#Role_ID_"+$(this).val()).html());
                    
                    var $key_value = $(this).val();
                    var $key_text = $("#Role_ID_"+$(this).val()).html();
                    
                    $.ajax({
                        type:   "POST",
                        url:    "{$_subdomain}/Data/userroles/delete/{$datarow.UserID|escape:'html'}",
                        data:   "RoleID=" + $(this).val(),
                        dataType: "html",
                        async: false,
                        success:    function(data, textStatus){
                        
                             $("#accessRightDropdown option[value='"+$key_value+"']").remove();
                             $('#RoleID_'+$key_value).remove();
                            
                             $('#accessRightDropdown')
                                .append($("<option></option>")
                                .attr("value", $key_value)
                                .text($key_text)); 
                            
                            //var obj = jQuery.parseJSON( data );
                            
                            
                        }
                    });                

                    $(this).slideUp();
                // ajax delete user_role record
            });
            
            $(document).on('click', '#Password', function(){
                $(this).val('');
            });
            
             /*$(document).on('change', '#accessRightDropdown', function(){
             
                 addAccessRight();
                
            });*/
            
            addAccessRight = function (){
            
                //ajax check db then added
                //console.info($('#accessRightDropdown').val() + ' ' + $('#SNFormPanel').height());
                if($('#accessRightDropdown').val() != ''){
                    var roleID = $('#accessRightDropdown option:selected').val();
                    
                    if('{$datarow.UserID|escape:'html'}' != ''){
                    $.ajax({
                        type:   "POST",
                        url:    "{$_subdomain}/Data/userroles/insert/{$datarow.UserID|escape:'html'}",
                        data:   "RoleID=" + roleID,
                        dataType: "html",
                        async: false,
                        success:    function(data, textStatus){
                            
                            insertAccessRightHTML(roleID, $('#accessRightDropdown option:selected').text());
                            $('#accessRightDropdown option:selected').remove();
                        }
                    });
                    } else {
                            console.info( 'roleID = ' + roleID );
                            $('#accessRight').parent().append('<input name="RoleID_'+roleID+'" id="RoleID_'+roleID+'"   value="'+roleID+'" type="hidden" />');
                            insertAccessRightHTML(roleID, $('#accessRightDropdown option:selected').text());
                            $('#accessRightDropdown option:selected').remove();                    
                    }

                }
                //$('#cboxLoadedContent').height( $('#SNFormPanel').height() );
                //$('#colorbox').height( $('#SNFormPanel').height() + 80 );
                
            }//end addAccessRight()
            


           /* =======================================================
            *
            * Initialise input auto-hint functions...
            *
            * ======================================================= */

            $('.auto-hint').focus(function() {
                    $this = $(this);
                    if ($this.val() == $this.attr('title')) {
                        $this.val('').removeClass('auto-hint');
                        if ($this.hasClass('auto-pwd')) {
                            $this.prop('type','password');
                        }
                    }
                } ).blur(function() {
                    $this = $(this);
                    if ($this.val() == '' && $this.attr('title') != '')  {
                        $this.val($this.attr('title')).addClass('auto-hint');
                        if ($this.hasClass('auto-pwd')) {
                            $this.prop('type','text');
                        }
                    }         
                } ).each(function(){
                    $this = $(this);
                    if ($this.attr('title') == '') { return; }
                    if ($this.val() == '') { 
                        if ($this.attr('type') == 'password') {
                            $this.addClass('auto-pwd').prop('type','text');
                        }
                        $this.val($this.attr('title')); 
                    } else { 
                        $this.removeClass('auto-hint'); 
                    }
                    $this.attr('autocomplete','off');
                } ); 
                
                
         });       
        
    </script>
    
    
    <div id="SNFormPanel" class="SystemAdminFormPanel" >
    
                <form id="SNForm" name="SNForm" method="post"  action="#" class="inline">
       
                <fieldset>
                    <legend title="" >{$form_legend|escape:'html'}</legend>
                        
                    <p><label id="suggestText"></label></p>
                            
                    <p>
                        <label ></label>
                        <span class="topText" >{$page['Text']['top_info_text1']|escape:'html'} <span>*</span> {$page['Text']['top_info_text2']|escape:'html'}</span>
                    </p>
                    <p>
                        <label class="fieldLabel" for="Username"> {$page['Labels']['username']|escape:'html'}:<sup>*</sup></label>
                        &nbsp;&nbsp; <input type="text" class="text" name="Username" value="{$datarow.Username|escape:'html'}" id="Username" >
                    </p>
                    <p>
                        <label class="fieldLabel" for="Password" >{$page['Labels']['password']|escape:'html'}:<sup>*</sup></label>
                        &nbsp;&nbsp; <input type="password" class="text"  name="Password" value="{$datarow.Password|escape:'html'}" id="Password" />          
                    </p>
                    {*<p>
                        <label class="fieldLabel" for="ComfirmPassword" >{$page['Labels']['confirm_password']|escape:'html'}:</label>
                        &nbsp;&nbsp; <input type="password" class="text"  name="ComfirmPassword" value="{$datarow.Password|escape:'html'}" id="ComfirmPassword" />          
                    </p>*}                    
                    <p>
                        <label class="fieldLabel" for="CustomerTitleID"> {$page['Labels']['title']|escape:'html'}:</label>

                        &nbsp;&nbsp; <select id="CustomerTitleID" name="CustomerTitleID">
                            <option value="">{$page['Text']['select_default_option']|escape:'html'}</option>
                            
                            {foreach $Titles as $title}
                                <option value="{$title.TitleID}" {if $datarow.CustomerTitleID eq $title.TitleID}selected="selected"{/if}>{$title.Name|escape:'html'}</option>
                            {/foreach}
                        </select>
                    </p>
                    <p>
                        <label class="fieldLabel" for="ContactFirstName"> {$page['Labels']['forename']|escape:'html'}:<sup>*</sup></label>
                        &nbsp;&nbsp; <input id="Forename" type="text" class="text" name="ContactFirstName" value="{$datarow.ContactFirstName|escape:'html'}" />
                    </p>   
                    <p>
                        <label class="fieldLabel" for="ContactLastName"> {$page['Labels']['surname']|escape:'html'}:</label>
                        &nbsp;&nbsp; <input type="text" class="text" id="Surname" name="ContactLastName" value="{$datarow.ContactLastName|escape:'html'}" />
                    </p>  
                    <p>
                        <label class="fieldLabel" for="ContactEmail">{$page['Labels']['email']|escape:'html'}:</label>
                        &nbsp;&nbsp; <input type="text" id="ContactEmail" name="ContactEmail" class="text" 
                                            value="{$datarow.ContactEmail|escape:'html'}" />
                    </p> 
                    <p>
                        <label class="fieldLabel" for="ContactMobile"> {$page['Labels']['mobile']|escape:'html'}:</label>
                        &nbsp;&nbsp; <input type="text" class="text" name="ContactMobile" value="{$datarow.ContactMobile|escape:'html'}" id="ContactMobile" />
                    </p>                      
                    <p>
                        <label class="fieldLabel" for="SecurityQuestionID"> {$page['Labels']['question']|escape:'html'}:<sup>*</sup></label>
                        &nbsp;&nbsp; <select id="SecurityQuestionID" name="SecurityQuestionID">
                            <option value="">{$page['Text']['select_default_option']|escape:'html'}</option>
                            {foreach $SecurityQuestions as $question}
                                <option value="{$question.SecurityQuestionID}" {if $datarow.SecurityQuestionID eq $question.SecurityQuestionID}selected="selected"{/if}>{$question.Question}</option>
                            {/foreach}
                        </select>
                    </p>   
                    <p>
                        <label class="fieldLabel" for="Answer">{$page['Labels']['answer']|escape:'html'}:<sup>*</sup></label>
                        &nbsp;&nbsp; <input type="text" id="Answer" name="Answer" class="text" 
                                            value="{$datarow.Answer|escape:'html'}" />
                    </p>  
                    
                    {if $datarow.SuperAdmin eq 1}
                    
                         <input type="hidden" name="UserType" id="UserType" value="" >
                         <input type="hidden" name="UserType_temp" id="UserType_temp" value="" >
                         <input type="hidden" name="CompanyID" id="CompanyID" value="" >
                         <input type="hidden" name="DefaultBrandID" id="DefaultBrandID" value="" >
                         <input type="hidden" name="SADefaultBrandID" id="SADefaultBrandID" value="{$datarow.DefaultBrandID|escape:'html'}" >
                        
                    {else}
                        <p>
                            <label class="fieldLabel">{$page['Labels']['user_type']|escape:'html'}:<sup>*</sup></label>
                            &nbsp;&nbsp; <select id="UserType" name="UserType">
                                <option value=""{if $UserType eq ''} selected="selected"{/if}>{$page['Text']['select_default_option']|escape:'html'}</option>
                            {foreach $UserTypes as $Type}
                                <option value="{$Type.Name}"{if $UserType eq $Type.Name} selected="selected"{/if}>{$Type.Value}</option>

                            {/foreach}
                            <input id="UserType_temp" value="{$UserType}" type="hidden" />
                            </select>
                        </p>
                        <div id="branch_job_search_scope_fields">
                        <p>
                            <label class="fieldLabel">{$page['Labels']['branch_job_search_scope']|escape:'html'}:</label>
                            &nbsp;&nbsp; <select id="BranchJobSearchScope" name="BranchJobSearchScope">
                                <option value=""{if $datarow.BranchJobSearchScope eq ''} selected="selected"{/if}>{$page['Text']['select_default_option']|escape:'html'}</option>
                            {foreach $SearchScopes  as $scope}
                                <option value="{$scope}"{if $datarow.BranchJobSearchScope eq $scope} selected="selected"{/if}>{$scope}</option>
                            {/foreach}
                            </select>
                        </p>
                        </div><!-- branch_job_search_scope -->
                        <div id="branch_job_network_fields">
                        <p>
                            <label class="fieldLabel" for="NetworkID">{$page['Labels']['network']|escape:'html'}:<sup>*</sup></label>
                            &nbsp;&nbsp; <select id="NetworkID" name="NetworkID">
                                <option value=""{if $datarow.NetworkID eq ''} selected="selected"{/if}>{$page['Text']['select_service_network']|escape:'html'}</option>
                            {foreach $networks  as $network}
                                <option value="{$network.NetworkID}"{if $datarow.NetworkID eq $network.NetworkID} selected="selected"{/if}>{$network.CompanyName}</option>
                            {/foreach}
                            </select>
                        </p>
                        </div>
                        <div id="branch_job_client_fields">
                        <p>
                            <label class="fieldLabel" for="ClientID">{$page['Labels']['client']|escape:'html'}:<sup>*</sup></label>
                            &nbsp;&nbsp; <select id="ClientID" name="ClientID">
                                <option value=""{if $datarow.ClientID eq ''} selected="selected"{/if}>{$page['Text']['select_default_option']|escape:'html'}</option>
                                {foreach $nClients as $client}
                                    <option value="{$client.ClientID}" {if $datarow.ClientID eq $client.ClientID}selected="selected"{/if}>{$client.ClientName|escape:'html'}</option>
                                {/foreach}
                            </select>
                        </p>
                        </div>
                        <p id="P_CompanyID">
                            <label class="fieldLabel" for="CompanyID" id="CompanyIDLabel" >{$page['Labels']['company']|escape:'html'}:<sup>*</sup></label>
                            &nbsp;&nbsp; <select id="CompanyID" name="CompanyID" disabled="disabled">
                                <option value="">{$page['Text']['select_default_option']|escape:'html'}</option>

                            </select>
                                <input id="CompanyID_temp" value="{$CompanyID}" type="hidden" />
                        </p>                    

                        <p id="P_DefaultBrandID">
                            <label class="fieldLabel">{$page['Labels']['brand']|escape:'html'}:<sup>*</sup></label>
                            &nbsp;&nbsp; <select id="DefaultBrandID" name="DefaultBrandID" disabled="disabled">
                                <option value="">{$page['Text']['select_default_option']|escape:'html'}</option>

                            </select>

                            <input id="DefaultBrandID_temp" value="{$datarow.DefaultBrandID}" type="hidden" />
                        </p>    
                        <div>
                            <div class="fieldLabel"> <label class="fieldLabel">{$page['Labels']['access_right']|escape:'html'}:<sup>*</sup></label> </div>
                            <div class="fieldInput">
                                <ul id="accessRight"></ul>       
                                <div style="clear:both"></div>
                                <select id="accessRightDropdown" name="AccessRight">
                                    <option value="">{$page['Text']['select_default_option']|escape:'html'}</option>
                                </select>
                               {* <span><a href="#" onclick="addAccessRight(); return false;"><img src="{$_subdomain}/css/Skins/{$_theme}/images/add_icon.png" width="20" height="20"  > </a></span>*}
                            </div>
                        </div>  
                            
                   {/if}         
                    <p>
                    <hr /> 
                    </p>

                          
                    <p>
                        <label class="fieldLabel" for="Status" >{$page['Labels']['status']|escape:'html'}:<sup>*</sup></label>

                        &nbsp;&nbsp; 

                        {foreach $statuses as $status}

                            <input type="radio" name="Status" value="{$status.Code}" {if $datarow.Status eq $status.Code} checked="checked" {/if}  /> 
                            <span class="text" >{$status.Name|escape:'html'}</span> 

                        {/foreach}    
                    </p>
                         
                         

                    <p>

                        <span class= "bottomButtons" >

                            <input type="hidden" name="UserID"  value="{$datarow.UserID|escape:'html'}" >
                            
                            <input type="hidden" name="SuperAdminFlag" id="SuperAdminFlag" value="{$datarow.SuperAdmin|escape:'html'}" >
                            

                            {if $datarow.UserID neq '' && $datarow.UserID neq '0'}

                                <input type="submit" name="update_save_btn" class="textSubmitButton centerBtn" id="update_save_btn"  value="{$page['Buttons']['save']|escape:'html'}" />

                            {else}

                                <input type="submit" name="insert_save_btn" class="textSubmitButton centerBtn" id="insert_save_btn"  value="{$page['Buttons']['save']|escape:'html'}" />

                            {/if}

                                <br />
                                <input type="submit" name="cancel_btn" class="textSubmitButton rightBtn" id="cancel_btn" onclick="return false;"  value="{$page['Buttons']['cancel']|escape:'html'}" />



                        </span>

                    </p>

                           
                          
                         
                         


                </fieldset>    
                        
                </form>        
                                             
       
</div>
                 
{/if}  
                                    
                                    
{* This block of code is for to display message after data updation *} 

                                    
<div id="dataUpdatedMsg" class="SystemAdminFormPanel" style="display: none;" >   
    
    <form id="dataUpdatedMsgForm" name="dataUpdatedMsgForm" method="post"  action="#" class="inline">
            <fieldset>
                <legend title="" > {$form_legend|escape:'html'} </legend>
                <p>
                 
                    <b>{$page['Text']['data_updated_msg']|escape:'html'}</b><br><br>
                 
                </p>
                
                <p>

                    <span class= "bottomButtons" >


                        <input type="submit" name="cancel_btn" class="textSubmitButton rightBtn" id="cancel_btn" onclick="return false;"  value="{$page['Buttons']['ok']|escape:'html'}" >

                    </span>

                </p>
                          
                
            </fieldset>   
    </form>            
    
    
</div>    

    
 
  {* This block of code is for to display message after data insertion *}                      
                        
<div id="dataInsertedMsg" class="SystemAdminFormPanel" style="display: none;" >   
    
    <form id="dataInsertedMsgForm" name="dataInsertedMsgForm" method="post"  action="#" class="inline">
            <fieldset>
                <legend title="" > {$form_legend|escape:'html'} </legend>
                <p>
                 
                    <b>{$page['Text']['data_inserted_msg']|escape:'html'}</b><br><br>
                 
                </p>
                
                <p>

                    <span class= "bottomButtons" >


                        <input type="submit" name="cancel_btn" class="textSubmitButton rightBtn" id="cancel_btn" onclick="return false;"  value="{$page['Buttons']['ok']|escape:'html'}" >

                    </span>

                </p>
                          
                
            </fieldset>   
    </form>            
    
    
</div>                            
                        
