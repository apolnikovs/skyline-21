{* <select name="PackagingType" id="bcfPackagingType" class="text" > *}
    <option value="" selected="selected">{$page['Text']['please_select']|escape:'html'}</option>
    {foreach $packaging_types as $packaging_type}
        <option value="{$packaging_type}" {if $packaging_type eq $collection['packaging_type']|default: ''}selected{/if}>{$packaging_type|upper|escape:'html'}</option>
    {/foreach}
{* </select> *}