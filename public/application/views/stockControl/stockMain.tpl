{extends "DemoLayout.tpl"}


{block name=config}
    {$Title = "Stock"}
    {$PageId = $StockPage}
    {$fullscreen=true}
    {$showTopLogoBlankBox=false}
    {$controller="StockControl"}
{/block}

{block name=afterJqueryUI}
    <script type="text/javascript" src="{$_subdomain}/js/jquery.combobox.js"></script>
    <script type="text/javascript" src="{$_subdomain}/js/TableTools.min.js"></script>
    <script type="text/javascript" src="{$_subdomain}/js/datatables.api.js"></script>
    <link rel="stylesheet" href="{$_subdomain}/css/themes/pccs/style.css" type="text/css" media="screen" charset="utf-8" />
{/block}


{block name=scripts}

  
 <script>
   
   
        
        
        
        var oldValue;
    $(document).ready(function() {
    var oTable = $('#StockResults').dataTable( {
"sDom": 'R<"left"><"top"f>t<"bottom"><"centered"><"right">pli<"clear">',
//"sDom": 'Rlfrtip',
"bServerSide": true,

		
    "sAjaxSource": "{$_subdomain}/{$controller}/loadStockTable",
     
                "fnServerData": function ( sSource, aoData, fnCallback ) {
			/* Add some extra data to the sender */
			aoData.push( { "name": "more_data", "value": "my_value" } );
			$.getJSON( sSource, aoData, function (json) { 
				/* Do whatever additional processing you want on the callback, then tell DataTables */
				fnCallback(json)
                                $('#tt-Loader').hide();
                                $('#StockResults').show();
                               
			} );
                        },
                        "oLanguage": {
                                "sLengthMenu": "_MENU_ Records per page",
                                "sSearch": "Search within results"
                            },
                        
"bPaginate": true,
"sPaginationType": "full_numbers",
"aLengthMenu": [[ 10, 15, 25, 50, 100 , -1], [10, 15, 25, 50, 100, "All"]],
"iDisplayLength" : 10,

"aoColumns": [ 
			
			
			{for $er=0 to $data_keys|@count-1}
                                {$vis=1}
                               
                               
                            {if $er==0}{$vis=0}{/if}
                            { "bVisible":{$vis} },
			 
                           {/for} 
                               
                               true
                            
		] 
               
   
 
        
          
});//datatable end
  
   /* Add a click handler to the rows - this could be used as a callback */
	$("#StockResults tbody").click(function(event) {
		$(oTable.fnSettings().aoData).each(function (){
			$(this.nTr).removeClass('row_selected');
		});
		$(event.target.parentNode).addClass('row_selected');
                var anSelected = fnGetSelected( oTable );
                var aData = oTable.fnGetData(anSelected[0]); // get datarow
                
    if (anSelected!="")  // null if we clicked on title row
    {
    
                $('#exportType').val(aData[0]);
                }
	});
        
      /* Get the rows which are currently selected */
function fnGetSelected( oTableLocal )
{
	var aReturn = new Array();
	var aTrs = oTableLocal.fnGetNodes();
	
	for ( var i=0 ; i<aTrs.length ; i++ )
	{
		if ( $(aTrs[i]).hasClass('row_selected') )
		{
			aReturn.push( aTrs[i] );
		}
	}
	return aReturn;
}



    /* Add a click handler for the edit row */
	$('button[id^=edit]').click( function() {
		var anSelected = fnGetSelected( oTable );
                var aData = oTable.fnGetData(anSelected[0]); // get datarow
    if (anSelected!="")  // null if we clicked on title row
    {
  //becouse all the data can be reordered and hidden any additinal info like ID needs to be stored in <tr> and accesed by anSelected[0].id where .id is data needet
$.colorbox({ 
 
                        href:"{$_subdomain}/{$controller}/processStock/id="+anSelected[0].id,
                        title: "Edit Stock",
                        opacity: 0.75,
                        width:800,
                        overlayClose: false,
                        escKey: false

                });
    }else{
    alert("Please select row first");
    }
		
	} );   
        
    /* Add a click handler for the orderStock button row */
	$('#orderStockButton').click( function() {
		var anSelected = fnGetSelected( oTable );
                var aData = oTable.fnGetData(anSelected[0]); // get datarow
    if (anSelected!="")  // null if we clicked on title row
    {
  //becouse all the data can be reordered and hidden any additinal info like ID needs to be stored in <tr> and accesed by anSelected[0].id where .id is data needet
$.colorbox({ 
 
                       href:"{$_subdomain}/{$controller}/orderStockForm/id="+anSelected[0].id,
                        title: "Order Stock",
                        opacity: 0.75,
                       
                        overlayClose: false,
                        escKey: false

                });
    }else{
    alert("Please select row first");
    }
		
	} );                            
    
    /* Add a click handler for the edit row */
	$('button[id^=stockHistoryBut]').click( function() {
		var anSelected = fnGetSelected( oTable );
                var aData = oTable.fnGetData(anSelected[0]); // get datarow
    if (anSelected!="")  // null if we clicked on title row
    {
  //becouse all the data can be reordered and hidden any additinal info like ID needs to be stored in <tr> and accesed by anSelected[0].id where .id is data needet
$.colorbox({ 
 
                        href:"{$_subdomain}/{$controller}/stockPartHistory/id="+anSelected[0].id,
                        title: "Stock History",
                        opacity: 0.75,
                        
                        overlayClose: false,
                        escKey: false

                });
    }else{
    alert("Please select row first");
    }
		
	} );                            
    
    
    
    
	$('input[id^=inactivetick]').click( function() {
         $('#unaprovedtick').attr("checked",false);
        if($(this).attr("checked")=="checked"){
                 $('#tt-Loader').show();
                                $('#StockResults').hide();
		 oTable.fnReloadAjax("{$_subdomain}/{$controller}/loadStockTable/inactive=1/");
                 }else{
                  $('#tt-Loader').show();
                                $('#StockResults').hide();
		 oTable.fnReloadAjax("{$_subdomain}/{$controller}/loadStockTable/");
                 }
	} );  
        var v=0;
	$('a[id^=showpendingtick]').click( function() {
        v++;
        if(v==1){
                 $('#tt-Loader').show();
                                $('#StockResults').hide();
		 oTable.fnReloadAjax("{$_subdomain}/{$controller}/loadStockTable/pending=1/");
                 }else{
                 v=0;
                  $('#tt-Loader').show();
                                $('#StockResults').hide();
		 oTable.fnReloadAjax("{$_subdomain}/{$controller}/loadStockTable/");
                 }
	} );                            
    
	$('input[id^=unaprovedtick]').click( function() {
        $('#inactivetick').attr("checked",false);
        if($(this).attr("checked")=="checked"){
                 $('#tt-Loader').show();
                                $('#StockResults').hide();
		 oTable.fnReloadAjax("{$_subdomain}/{$controller}/loadStockTable/unaproved=1/");
                 }else{
                  $('#tt-Loader').show();
                                $('#StockResults').hide();
		 oTable.fnReloadAjax("{$_subdomain}/{$controller}/loadStockTable/");
                 }
	} );                            
          
          
          
          
 /* Add a click handler for the delete row */
	$('button[id^=delete]').click( function() {
		var anSelected = fnGetSelected( oTable );
                var aData = oTable.fnGetData(anSelected[0]); // get datarow
    if (anSelected!="")  // null if we clicked on title row
    {
  //becouse all the data can be reordered and hidden any additinal info like ID needs to be stored in <tr> and accesed by anSelected[0].id where .id is data needet
if (confirm('Are you sure you want to delete this entry from database?')) {
    window.location="{$_subdomain}/{$controller}/deleteStock/id="+anSelected[0].id
} else {
    // Do nothing!
}


    }else{
    alert("Please select row first");
    }
		
	} );                            
             

/* Add a dblclick handler to the rows - this could be used as a callback */
	$("#StockResults  tbody").dblclick(function(event) {
		$(oTable.fnSettings().aoData).each(function (){
			$(this.nTr).removeClass('row_selected');
		});
		$(event.target.parentNode).addClass('row_selected');
            var anSelected = fnGetSelected( oTable );
                var aData = oTable.fnGetData(anSelected[0]); // get datarow
    if (anSelected!="")  // null if we clicked on title row
    {
$.colorbox({ 
 
                        href:"{$_subdomain}/{$controller}/processStock/id="+anSelected[0].id,
                        title: "Edit Stock",
                        opacity: 0.75,
                        width:800,
                        overlayClose: false,
                        escKey: false

                });
    }else{
    alert("Please select row first");
    }
		
	} );  




	
		
  $('#serviceProviderSelect').change( function() {
        
        if($(this).val()!="0"){
                 $('#tt-Loader').show();
                                $('#StockResults').hide();
		 oTable.fnReloadAjax("{$_subdomain}/{$controller}/loadStockTable/spid="+$(this).val()+"/");
                 }else{
                  $('#tt-Loader').show();
                                $('#StockResults').hide();
		 oTable.fnReloadAjax("{$_subdomain}/{$controller}/loadStockTable/");
                 }
	} );  
        
        
        
        //filter apply
  $('#ModelsFilter, #locationFilter, #supplierFilter, #categoryFilter, #shelfFilter').change( function() {
        
       
    oTable.fnReloadAjax("{$_subdomain}/{$controller}/loadStockTable/"+$('#StockControlFilterForm').serialize().replace(/&/g,'/')+"/");
                
	} );  
        
   $('#manufacturerFilter').change( function() {
        
        
                
                 
		 $.post("{$_subdomain}/StockControl/getSPManufacturerModels",{ id: $(this).val()},
function(data) {
           
             en=jQuery.parseJSON(data);
            
             $("select[id='ModelsFilter'] option:gt(0)").remove();
           $.each(en, function(key,val) {
         
             $('#ModelsFilter').append(
        $('<option></option>').val(val.ServiceProviderModelID).html(val.ModelNumber)
    );
             });
             
             
	} );  
	} );  

} );//doc ready

//displaying table pref colorbox
function showTablePreferences(){

$.colorbox({ 
 
                        href:"{$_subdomain}/{$controller}/tableDisplayPreferenceSetup/page=stock/table=sp_part_stock_template",
                        title: "Table Display Preferences",
                        opacity: 0.75,
                        overlayClose: false,
                        escKey: false

                });
}
function showStockLevels(){

$.colorbox({ 
 
                        href:"{$_subdomain}/StockControl/getStockLevels",
                        title: "Stock Levels",
                        opacity: 0.75,
                        overlayClose: false,
                        escKey: false,
                        width:600

                });
}

function stockInsert()
{
$.colorbox({ 
 
                        href:"{$_subdomain}/{$controller}/processStock/",
                        title: "Insert Stock",
                        opacity: 0.75,
                        width:800,
                        overlayClose: false,
                        escKey: false

                });

}
function stockEdit()
{
    var anSelected = fnGetSelected( oTable );
                var aData = oTable.fnGetData(anSelected[0]); 
                alert(aData);
$.colorbox({ 
 
                        href:"{$_subdomain}/{$controller}/processStock/id="+aData,
                        title: "Edit Stock",
                        opacity: 0.75,
                        width:800,
                        overlayClose: false,
                        escKey: false

                });

}


                
   function toggleFilters(){
   $('#StockControlFilterDiv').toggle('slow', function() {
 
   if($('#StockControlFilterDiv').is(":hidden")){
  
   $('#toggleFiltersa').html('Show Filters');
   }else{
   
   $('#toggleFiltersa').html('Hide Filters');
   }
  });
   
   }
                
                

    </script>
<link rel="stylesheet" href="{$_subdomain}/css/themes/pccs/style.css" type="text/css" />
    
{/block}


{block name=body}
 <div style="float:right">
         <a href="#" onclick="showTablePreferences();">Display Preferences</a>
            </div>
    <div class="breadcrumb" style="width:100%">
        <div>

            <a href="{$_subdomain}/index/siteMap/" >Site Map</a> / Stock Control

        </div>
    </div>
           
    <div class="main" id="home" style="width:100%">

               <div class="ServiceAdminTopPanel" >
                    <form id="StockTopForm" name="StockTopForm" method="post"  action="#" class="inline">

                        <fieldset>
                        <legend title="" >Stock Control</legend>
                        <p>
                            <label>{$page['Text']['legend_text']}</label>
                        </p> 

                        </fieldset> 


                    </form>
                </div>  
                       <div style="float:right">
         <a href="#" id="toggleFiltersa" onclick="toggleFilters();">Hide Filters</a>
            </div>
         <div id="StockControlFilterDiv" style="margin-top:10px;;margin-bottom: 145px;">
             <form id="StockControlFilterForm">
             <div style="float:left;position: relative;">
                 <p style="font-weight: bold">Search</p>
                 <p>
                     <select  id="locationFilter" name="locationFilter">
                         <option value="">Select Location</option>
                     </select>
                 </p>
                 <p>
                     <select  id="supplierFilter" name="supplierFilter">
                         <option value="">Select Supplier</option>
                         {foreach $supplierList as $s}
                            <option value="{$s.ServiceProviderSupplierID}">{$s.CompanyName|default:''}</option>
                        {/foreach}
                     </select>
                 </p>
                 <p>
                     <select  id="categoryFilter" name="categoryFilter">
                         <option value="">Select Category</option>
                     </select>
                 </p>
                 <p>
                     <select  id="shelfFilter" name="shelfFilter">
                         <option value="">Select Shelf Location</option>
                     </select>
                 </p>
                 {if isset($splist)}
                     <p>
                        <select id="serviceProviderSelect" >
                            <option value="0">Select Service Provider</option>
                        {foreach $splist as $s}
                            <option value="{$s.ServiceProviderID}">{$s.Acronym|default:''}</option>
                        {/foreach}
                        </select>
                        </p>
                        {/if}
             </div>
             <div style="float:left;margin-left: 40px;">
                 <p style="font-weight: bold">Model</p>
                 <p>
                     <select  id="manufacturerFilter" name="manufacturerFilter">
                         <option value="">All Manufacturers</option>
                         {foreach $manufacturerList as $s}
                            <option value="{$s.ServiceProviderManufacturerID}">{$s.ManufacturerName|default:''}</option>
                        {/foreach}
                     </select>
                 </p>
                 <p>
                     <select  id="ModelsFilter" name="ModelsFilter">
                         <option value="">All Models</option>
                          {foreach $ModelList as $s}
                            <option value="{$s.ServiceProviderModelID}">{$s.ModelNumber|default:''}</option>
                        {/foreach}
                     </select>
                 </p>
                 
             </div>
             <div style="float:left;margin-left: 40px;">
                 <p style="font-weight: bold">Filter</p>
                 <p><input checked="checked" type="radio" value="All" name="sundryFilter"> All</p>
                 <p><input type="radio" name="sundryFilter"> Exclude Sundry</p>
                 <p><input type="radio" name="sundryFilter"> Sundry Only</p>
             </div>
             <div style="float:left;margin-left: 40px;">
                 <p style="font-weight: bold">&nbsp;</p>
                 <p><input checked="checked" type="radio" value="All" name="accesoriesFilter"> All</p>
                 <p><input type="radio" name="accesoriesFilter"> Exclude Accessories</p>
                 <p><input type="radio" name="accesoriesFilter"> Accessories Only</p>
             </div>
                  </form>
         </div>    
    
                {if !isset($NotPermited)}        
 <div class="ServiceAdminResultsPanel" id="StockResultsPanel" >
     
     
                    
<form id="StockResultsForm" class="dataTableCorrections">
    
                        
                        <table id="StockResults" style="display:none" border="0" cellpadding="0" cellspacing="0" class="browse" >
                        <thead>
			    <tr>
                                {foreach from=$data_keys key=kk item=vv}
                                  
                                    <th>
                                        {$vv}
                                    </th>
                                  
                                {/foreach}
                                <th style="width:10px"></th>
			    </tr>
                        </thead>
                        <tbody>
                           <div style="text-align:left" id="tt-Loader"><img src="{$_subdomain}/images/ajax-loading_medium.gif"></div>
                        </tbody>
                    </table>  
                    <input type="hidden" name="sltSPs" id="sltSPs" >            
                    

</form>
                    
                    
                    
                </div>        

                <div class="bottomButtonsPanelHolder" style="position: relative;">
                    <div class="bottomButtonsPanel" style="position:absolute;top:-65px;width:500px">
                        <div style="float:left">
                            <button type="button" id="edit"  class="gplus-blue">{$page['Buttons']['edit']|escape:'html'}</button>
                            <button type="button" onclick="stockInsert()" class="gplus-blue">{$page['Buttons']['insert']|escape:'html'}</button>
                            <button type="button" id="delete" class="gplus-red">{$page['Buttons']['delete']|escape:'html'}</button>
                        </div>
<!--                            <div id="buttonSpacer" style="width:100px;float:left">&nbsp;</div><button type="button" id="stockHistoryBut" class="gplus-blue">Stock History</button>-->
                    </div>
                  
                    <div style="width:100%;text-align: right">
                        <input id="inactivetick"  type="checkbox" > Show Inactive&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>
                  
                    </div>
                        
                        
                 <div style="float:left"> 
                     <p>
                         <button style="width:150px" class="gplus-blue" type="button">Add Stock</button>
                         <button id="orderStockButton" style="width:150px;margin-left:5px;" class="gplus-blue" type="button">Order Stock</button>
                         <button style="width:150px;margin-left:5px;" class="gplus-blue" type="button">Stock Return</button>
                         <button style="width:150px;margin-left:5px;" class="gplus-blue" type="button">Audit Stock</button>
                         <button style="width:150px;margin-left:5px;" class="gplus-blue" type="button">Minimum Stock Order</button>
                         <button onclick="showStockLevels()" style="width:150px;margin-left:5px;" class="gplus-blue" type="button">Stock Levels</button>
                     
                     </p>  
                     <p>
                         <button style="width:150px" class="gplus-blue" type="button">Deplete Stock</button>
                         <button style="width:150px;margin-left:5px;" class="gplus-blue" type="button">Stock Transfer</button>
                         <button id="stockHistoryBut" style="width:150px;margin-left:5px;" class="gplus-blue" type="button">Stock History</button>
                         <button style="width:150px;margin-left:5px;" class="gplus-blue" type="button">Auto Re-Order</button>
                         <button style="width:150px;margin-left:5px;" class="gplus-blue" type="button">Print Label</button>
                         <button style="width:150px;margin-left:5px;" class="gplus-blue" type="button">Location Only</button>
                     
                     </p>  
                        
                 </div>
             
                  
                </div>        
                <div class="centerInfoText" id="centerInfoText" style="display:none;" ></div> 

                

                {if $SuperAdmin eq false} 

                   <input type="hidden" name="addButtonId" id="addButtonId" > 

                {/if} 
               
             {/if}   <!-- end of notpermited-->

    </div>
                <div>
                <hr>
                <button class="gplus-blue" style="float:right" type="button" onclick="window.location='{$_subdomain}/index/siteMap'">Finish</button>
                </div>
    


{/block}



