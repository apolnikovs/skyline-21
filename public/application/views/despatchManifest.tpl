<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title></title>

<style>
        
    #wrapper {
	width: 960px;
	margin: auto 0px;
	display: block;
	position: relative;
	border: 1px solid black;
	float: left;
	font-family: Arial,Helvetica,sans-serif;
	font-size: 11px;
    }
    
    #logo {
	outline: none; 
	text-decoration: none; 
	display: block;
	position: relative;
	float: left;
    }
    
    #logoDiv {
	position: relative;
	display: block;
	float: left;
	width: 420px;
    }
    
    h1 {
	color: #039CDF;
	font-family: Arial,Helvetica,sans-serif;
	font-size: 16px;
	font-weight: bold;
	line-height: 18px;
	text-transform: uppercase;
    }
    
</style>

</head>

<body>

    <div id="wrapper">
	
	<div id="header">
	    <div id="logoDiv">
		<img id="logo" src="{$brandLogo|escape:'html'}" />
	    </div>
	    <h1 style="display:block; position:relative; float:right; margin-top:50px;">Service Provider Manifest</h1>
	    <div style="clear:both; width:100%; border-bottom:5px solid #336699;">
		<p style="display:block; position:relative; float:right; text-align:right; margin-right:10px;">
		    Date Created: {date("d m Y")} &nbsp;&nbsp; Created by: {$userName}
		</p>
	    </div>
	</div>
	
    </div>
    
</body>
</html>
