<div calss="StockCodeSearch" id="StockCodeSearch" >
<form id="StockCodeSearchForm" name="StockCodeSearchForm" method="post"  action="#" class="inline">

    <fieldset>
    <legend> {$search_form_title} </legend>

        <table id="search_results" border="0" cellpadding="0" cellspacing="0" class="browse">
	<thead>
		<tr>
			<th width="33%" >{$page['Text']['stock_code']|escape:'html'}</th>
			<th width="33%" >{$page['Text']['model_no']|escape:'html'}</th>
			<th width="33%" >{$page['Text']['product_description']|escape:'html'}</th>
                        <th>{$page['Text']['product_type']|escape:'html'}</th>
                        <th>{$page['Text']['manufacturer']|escape:'html'}</th>
		</tr>
	</thead>
	<tbody>
            
        </tbody>
        </table>    
        
    </fieldset>
</form>
                
<div class="bottomButtonsPanel" >
    <hr>

    <button id="finish_btn" type="button" class="gplus-blue rightBtn" ><span class="label">{$page['Buttons']['finish']|escape:'html'}</span></button>
</div>  
</div>
   
