{extends "DemoLayout.tpl"}

{block name=config}
{$PageId = $PerformancePage}
{/block}

{block name=scripts}
    <link href="{$_subdomain}/demo/css/layout.css" rel="stylesheet">
    <link href="{$_subdomain}/demo/css/bootstrap.css" rel="stylesheet">
    <!-- link href="{$_subdomain}/demo/css/custom.css" rel="stylesheet" -->
    <link href="{$_subdomain}/demo/css/performance.css" rel="stylesheet">


    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script> 
    <script type="text/javascript" src="{$_subdomain}/demo/js/raphael-min.js"></script>
    <script type="text/javascript" src="{$_subdomain}/demo/js/pccs.gauge.js" ></script>
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript" src="{$_subdomain}/demo/js/jquery.slidePanel.min.js"></script>
    <script type="text/javascript" src="{$_subdomain}/js/jquery.colorbox-min.js"></script>
    <script type="text/javascript" src="{$_subdomain}/js/jquery.dataTables.1.9.4.min.js"></script>
    <script type="text/javascript" src="{$_subdomain}/js/jquery.dataTablesPCCS.js"></script>

        
    <script type="text/javascript" src="/js/jquery-ui-1.9.1.custom.min.js"></script>
    <link rel="stylesheet" href="/css/themes/pccs/jquery-ui-1.9.1.custom.min.css" type="text/css" />
        
    <script type="text/javascript">
    $(document).ready(function() {
        $('a').click(function() {
                        $('#main').html('<div class="preloader"><h1>Calculating....</h1></div> ');
                });
            });
            
    </script>
{/block}

{block name=body}

{* Certain tabs all use the same controller (with the tab as a paramneter) for these we set a higher tab variable, for those in their own controller we don't *}
{if (($tab == 'OpenJobs') || ($tab == 'ClosedJobs') || ($tab == 'OpenTAT'))}{$h_tab = 'JobsGauges/'}{else}{$h_tab = ''}{/if}

<div class="breadcrumb">
    <div>

        <a href="{$_subdomain}/index/index">{$page['Text']['home_page']}</a> /
        {if $tab == 'OpenTAT'}
            <a href="/Performance/JobsGauges/OpenTAT/">Graphical Analysis</a>
        {else}
            {$page['Text']['page_title']}
        {/if}
        / <a href="{$_subdomain}/Performance/{$h_tab}{$tab}{if $graphtype != 0}/graphtype={$graphtype}{/if}{if $graphdatasrc != 0}/graph={$graphdatasrc}{/if}">{$tab|replace:'Jobs':' Jobs'}</a> 
        {if $nid != 0}
        /  <a href="{$_subdomain}/Performance/{$h_tab}{$tab}{if $nid != 0}/nid={$nid}{/if}{if $graphtype != 0}/graphtype={$graphtype}{/if}{if $graphdatasrc != 0}/graph={$graphdatasrc}{/if}/showguage={$showguage}">{$n_name|cat:' '|lower|capitalize}</a>
        {/if}
        {if $cid != 0}
        / <a href="{$_subdomain}/Performance/{$h_tab}{$tab}{if $nid != 0}/nid={$nid}{/if}{if $cid != 0}/cid={$cid}{/if}{if $graphtype != 0}/graphtype={$graphtype}{/if}{if $graphdatasrc != 0}/graph={$graphdatasrc}{/if}/showguage={$showguage}">{$c_name|cat:' '|lower|capitalize}</a>
        {/if}
        {if $bid != 0}
        / <a href="{$_subdomain}/Performance/{$h_tab}{$tab}{if $nid != 0}/nid={$nid}{/if}{if $cid != 0}/cid={$cid}{/if}{if $bid != 0}/bid={$bid}{/if}{if $graphtype != 0}/graphtype={$graphtype}{/if}{if $graphdatasrc != 0}/graph={$graphdatasrc}{/if}/showguage={$showguage}">{$b_name|cat:' '|lower|capitalize}</a>
        {/if}
        {if $spid != 0}
        / <a href="{$_subdomain}/Performance/{$h_tab}{$tab}{if $spid != 0}/spid={$spid}{/if}{if $nid != 0}/nid={$nid}{/if}{if $cid != 0}/cid={$cid}{/if}{if $bid != 0}/bid={$bid}{/if}{if $graphtype != 0}/graphtype={$graphtype}{/if}{if $graphdatasrc != 0}/graph={$graphdatasrc}{/if}/showguage={$showguage}">{$sp_name|cat:' '|lower|capitalize|replace:'Tv ':'TV '}</a>
        {/if}
    </div>
</div>
    
{include file='include/menu.tpl'}

<div class="main" id="home" style="background: #ffffff;"> 
    <div class="performance">
        <div id="menu-panel">
            <div class="nav-center"><!-- navigation -->
                {if $tab != 'OpenTAT' && $tab != 'ClosedTAT' } {* Joe requested Open TAT to be moved from Performance tab to Graphical Analysis Tab *}
                <ul class="nav nav-tabs">
                    <li {if $tab != 'OpenJobs'}class="active"{/if}><a href="{$_subdomain}/Performance/JobsGauges/OpenJobs{if $spid != 0}/spid={$spid}{/if}{if $nid != 0}/nid={$nid}{/if}{if $cid != 0}/cid={$cid}{/if}{if $bid != 0}/bid={$bid}{/if}{if $graphtype != '0'}/graphtype={$graphtype}{/if}{if $graphdatasrc != '0'}/graph={$graphdatasrc}{/if}">Open Jobs</b>{if $tab == 'OpenJobs'}<b class="caret"></b>{/if}</a>
                    <li {if $tab != 'ClosedJobs'}class="active"{/if}><a href="{$_subdomain}/Performance/JobsGauges/ClosedJobs{if $spid != 0}/spid={$spid}{/if}{if $nid != 0}/nid={$nid}{/if}{if $cid != 0}/cid={$cid}{/if}{if $bid != 0}/bid={$bid}{/if}{if $graphtype != '0'}/graphtype={$graphtype}{/if}{if $graphdatasrc != '0'}/graph={$graphdatasrc}{/if}">Closed Jobs</b>{if $tab == 'ClosedJobs'}<b class="caret"></b>{/if}</a>
                    <li {if $tab != 'Geographic'}class="active"{/if}><a href="{$_subdomain}/Performance/Geographic{if $spid != 0}/spid={$spid}{/if}">Geographic</b>{if $tab == 'Geographic'}<b class="caret"></b>{/if}</a>
                    <li class="active"><a href="{$_subdomain}/index/performance/hot_spots">Hot Spots</a></li>
                    <li class="active"><a href="{$_subdomain}/index/performance/overdue_jobs">Overdue Jobs</a></li>
                    <li {if $tab != 'Segmentation'}class="active"{/if}><a href="{$_subdomain}/Performance/Segmentation">Segmentation</a></li>
                    <li class="active"><a href="{$_subdomain}/index/performance/Wallboard">Wallboard</a></li>
                    <li class="active"><a href="{$_subdomain}/index/performance/Report">Report</a></li>
                </ul>
                {else} {*Graphical Anlaysis Tab sub tabs *}
                    <ul class="nav nav-tabs">
                        <li {if $tab == 'OpenTAT'}class="active"{/if}><a href="{$_subdomain}/Performance/JobsGauges/OpenTAT{if $spid != 0}/spid={$spid}{/if}{if $nid != 0}/nid={$nid}{/if}{if $cid != 0}/cid={$cid}{/if}{if $bid != 0}/bid={$bid}{/if}{if $graphtype != '0'}/graphtype={$graphtype}{/if}{if $graphdatasrc != '0'}/graph={$graphdatasrc}{/if}">Open Jobs</b>{if $tab == 'OpenJobs'}<b class="caret"></b>{/if}</a>
                        <li {if $tab == 'ClosedTAT'}class="active"{/if}><a href="{$_subdomain}/Performance/JobsGauges/ClosedTAT{if $spid != 0}/spid={$spid}{/if}{if $nid != 0}/nid={$nid}{/if}{if $cid != 0}/cid={$cid}{/if}{if $bid != 0}/bid={$bid}{/if}{if $graphtype != '0'}/graphtype={$graphtype}{/if}{if $graphdatasrc != '0'}/graph={$graphdatasrc}{/if}">Closed Jobs</b>{if $tab == 'ClosedJobs'}<b class="caret"></b>{/if}</a>
                    </ul>
                {/if}

      
                
                <!-- begining of breadcrumbs and login -->
                <ul class="bootstrap_breadcrumb"><!-- This was the old breadcrumb designed when this was to be a seperate application. -->
                    {block name="filters"}&nbsp;{/block}
                </ul>
            </div><!-- end of navigation -->
        </div>
    </div>
</div>

{block name=PerformanceBody}
{/block}
                               
{/block}