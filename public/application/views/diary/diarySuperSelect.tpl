<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
 <head>


{block name=config}
{$Title = ""}
{$PageId = 88}



{/block}

{block name=scripts}

        <link rel="stylesheet" href="{$_subdomain}/css/colorbox/colorbox.css" type="text/css" charset="utf-8" /> 
        <link rel="stylesheet" href="{$_subdomain}/css/contextMenu/jquery.contextMenu.css" type="text/css" charset="utf-8" /> 
        
       <script type="text/javascript" src="{$_subdomain}/js/jquery-1.7.1.js"></script>
        <script type="text/javascript" src="{$_subdomain}/js/jquery.jlabel-1.3.min.js"></script> 
        <script type="text/javascript" src="{$_subdomain}/js/jquery.form.min.js"></script>
        <script type="text/javascript" src="{$_subdomain}/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="{$_subdomain}/js/jquery.dataTablesPCCS.js"></script>
        <script type="text/javascript" src="{$_subdomain}/js/jquery.colorbox-min.js"></script>
        <script type="text/javascript" src="{$_subdomain}/js/jquery.contextMenu.js"></script>
       
      
<!--        <script type="text/javascript" src="https://www.google.com/jsapi"></script>-->
<script>
    
    $(document).ready(function() {
    var modedd={if isset($mode)}{$mode}{else}0{/if};
    $.colorbox({ html:$('#h12s').html(), title:"Please select Service Provider",escKey: false,
    overlayClose: false,
                        
                        onClosed:function(){
                       if(modedd==1){
                        window.location="{$_subdomain}/Diary/default/table=1";
                        }
                       }
});
//$('#cboxClose').remove();
$('#cboxLoadedContent').css('overflow','hidden');
//$('#cboxLoadedContent').css('height','500px');
});
function setSp(n){
window.location="{$_subdomain}/Diary/default/setSp="+n;
}
</script>
</head>
        <body>
{/block}

    <div style="text-align:center;display:none;color:#6b6b6b;overflow:hidden" id="h12s" >
        <div >
    <h1 style="color:#666666; font-size: 16px;">Please select Service Provider</h1>
    <div style="color:#6b6b6b">
        <select onchange="setSp($(this).val())">
            <option></option>
        {foreach $spList as $s}
            <option value="{$s.ServiceProviderID}">{$s.CompanyName}</option>
            {/foreach}
        </select></div>
    </div>
    </div>


</body>