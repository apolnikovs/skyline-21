{extends "DemoLayout.tpl"}

{block name=config}
    {$Title = $page['Text']['page_title']|escape:'html'}
    {$PageId = $JobFaultCodesLookupsPage}
    {$fullscreen=true}
{/block}
{block name=afterJqueryUI}
    <script type="text/javascript" src="{$_subdomain}/js/jquery.combobox.js"></script>
    <script type="text/javascript" src="{$_subdomain}/js/TableTools.min.js"></script>
    <script type="text/javascript" src="{$_subdomain}/js/datatables.api.js"></script>
    <script type="text/javascript" src="{$_subdomain}/js/jquery.multiselect.js"></script>
    <link rel="stylesheet" href="{$_subdomain}/css/themes/pccs/style.css" type="text/css" media="screen" charset="utf-8" />
    <link rel="stylesheet" href="{$_subdomain}/css/themes/pccs/jquery.multiselect.css" type="text/css" />
    <style type="text/css" >
        .ui-combobox-input {
            width:280px;
        }
    </style>
{/block}

{block name=scripts}
<script type="text/javascript" src="{$_subdomain}/js/ColReorder.js"></script>
<script type="text/javascript">
var $statuses = [
    {foreach from=$statuses item=st}
        ["{$st.Name}", "{$st.Code}"],
    {/foreach}
];
function gotoEditPage($sRow)
{
    $('#updateButtonId').removeAttr('disabled').removeClass('gplus-blue-disabled').addClass('gplus-blue');
    $('#updateButtonId').trigger('click');
}
function showTablePreferences(){
    $.colorbox({
        href:"{$_subdomain}/LookupTables/tableDisplayPreferenceSetup/page=jobFaultCodesLookups/table=job_fault_code_lookup",
        title: "Table Display Preferences",
        opacity: 0.75,
        overlayClose: false,
        escKey: false
    });
}
$(document).ready(function() {
    $("#serviceProviderFilter").combobox({
        change: function() {
            $(location).attr('href', '{$_subdomain}/LookupTables/jobFaultCodesLookups/'+urlencode($("#serviceProviderFilter").val()));
        }
    });
    var displayButtons = "UAD";
    $('#JobFaultCodesLookupsResults').PCCSDataTable( {
     /*  aoColumns:[
                      { "mDataProp": "JobFaultCodeLookupID", " bVisible":   false },
            { "mDataProp": "LookupName"},
            { "mDataProp": "Description" },
            { "mDataProp": "ManufacturerID"},
            { "mDataProp": "ExcludeFromBouncerTable"},
            { "mDataProp": "ForcePartFaultCode"},
            { "mDataProp": "JobTypeAvailability" },
            { "mDataProp": "PromptForExchange"},
            { "mDataProp": "Status"},
             { "mData" : null, "bSortable": false, "sWidth" : "20px",
                            "mRender": function ( data, type, full ) {
                            return '<input class="taggedRec" type="checkbox" onclick="countTagged()" value="'+full.CountyID+'" name="check'+full.CountyID+'">';
                             }
                             }
       ], */
    
    
            aoColumns: [ 
            /* JobFaultCodeLookupID */{ bVisible:   false },
            /* LookupName */null,
            /* Description */null,
            /* ManufacturerID */null,
            /* ExcludeFromBouncerTable */null,
            /* ForcePartFaultCode */null,
            /* JobTypeAvailability */null,
            /* PromptForExchange */null,
            /* Status */null,
              { "mData" : null, "bSortable": false, "sWidth" : "20px",
                            "mRender": function ( data, type, full ) {
                            return '<input class="taggedRec" type="checkbox" onclick="countTagged()" value="'+full.CountyID+'" name="check'+full.CountyID+'">';
                             }
                             }
        ], 
        displayButtons:	displayButtons,
        bServerSide: false,
        addButtonId:	'addButtonId',
        addButtonText:	'{$page['Buttons']['insert']|escape:'html'}',
        createFormTitle:	'{$page['Text']['insert_page_legend']|escape:'html'}',
        createAppUrl:	'{$_subdomain}/LookupTables/jobFaultCodesLookups/insert/',
        createDataUrl:	'{$_subdomain}/LookupTables/ProcessData/JobFaultCodesLookups/',
        formInsertButton:	'insert_save_btn',
        deleteButtonId:	"deleteButtonID",
        aaSorting: [[ 1, "asc" ]],
        deleteAppUrl:	'{$_subdomain}/LookupTables/jobFaultCodesLookups/deleteForm/',
        deleteDataUrl:	'{$_subdomain}/LookupTables/ProcessData/JobFaultCodesLookups/delete/',
        formDeleteButton:	'delete_save_btn',
        frmErrorRules: {
            LookupName: { required: true },
            {if $SupderAdmin eq true}
                ServiceProviderID: { required: true },
            {/if}
            Description:{ required: true },
            PartFaultCodeNo:{ 
                    required: function(element){
                        if($("#ForcePartFaultCode").is(':checked'))
                            return true;
                        else
                            return false;
                    }
            },
            ManufacturerID:{ required: true }
        },
        frmErrorMessages: {
            LookupName: { required: "{$page['Errors']['lookupname_error']|escape:'html'}" },
            {if $SupderAdmin eq true}
                ServiceProviderID: { required: "{$page['Errors']['serviceProvider_error']|escape:'html'}" },
            {/if}
            Description:{ required: "{$page['Errors']['description_error']|escape:'html'}" },
            PartFaultCodeNo:{  required: "{$page['Errors']['partfaultcode_error']|escape:'html'}" },
            ManufacturerID:{ required: "{$page['Errors']['manufacturer_error']|escape:'html'}" }
        },
        popUpFormWidth:	715,
        popUpFormHeight:	430,
        updateButtonId:	'updateButtonId',
        updateButtonText:	'{$page['Buttons']['edit']|escape:'html'}',
        updateFormTitle:	'{$page['Text']['update_page_legend']|escape:'html'}',
        updateAppUrl:	'{$_subdomain}/LookupTables/jobFaultCodesLookups/update/',
        updateDataUrl:	'{$_subdomain}/LookupTables/ProcessData/JobFaultCodesLookups/',
        formUpdateButton:	'update_save_btn',
        colorboxFormId:	"CForm",
        frmErrorMsgClass:	"fieldError",
        frmErrorElement:	"label",
        htmlTablePageId:	'JobFaultCodesLookupsResultsPanel',
        htmlTableId:	'JobFaultCodesLookupsResults',
        fetchDataUrl:	'{$_subdomain}/LookupTables/ProcessData/JobFaultCodesLookups/fetch/'+urlencode("{$sId}")+'/'+"{$dataStatus}",
        formCancelButton:	'cancel_btn',
        dblclickCallbackMethod: 'gotoEditPage',
        //fnRowCallback:      'inactiveRow',
        searchCloseImage:	'{$_subdomain}/css/Skins/{$_theme}/images/close.png',
        tooltipTitle:	"{$page['Text']['tooltip_title']|escape:'html'}",
        iDisplayLength:	25,
        formDataErrorMsgId: "suggestText",
        frmErrorSugMsgClass:"formCommonError"
    });

/*var oTable = $('#JobFaultCodesLookupsResults').dataTable( {
"sDom": 'R<"left"><"top"f>t<"bottom"><"centered"><"right">pli<"clear">',
//"sDom": 'Rlfrtip',

"bServerSide": true,

		fnRowCallback:  function(nRow, aData){
               
                
                },
    "sAjaxSource": "{$_subdomain}/LookupTables/loadJobFaultCodesLookupsTable",
     
                "fnServerData": function ( sSource, aoData, fnCallback ) {
			aoData.push( { "name": "more_data", "value": "my_value" } );
			$.getJSON( sSource, aoData, function (json) { 
				fnCallback(json)
                                $('#tt-Loader').hide();
                                $('#JobFaultCodesLookupsResults').show();
                               
			} );
                        },
                        
"bPaginate": true,
"sPaginationType": "full_numbers",
"aLengthMenu": [[ 25, 50, 100 , -1], [25, 50, 100, "All"]],
"iDisplayLength" : 25,
 "aoColumns": [ 
			{for $er=0 to $data_keys|@count-1}
                                {$vis=1}
                               
                               
                            {if $er==0}{$vis=0}{/if}
                            { "bVisible":{$vis} },
			 
                           {/for} 
                               { "bVisible":1,"bSortable":false }
                               
                            
		] 
   
 
        
          
});//datatable end
	$("#JobFaultCodesLookupsResults tbody").click(function(event) {
		$(oTable.fnSettings().aoData).each(function (){
			$(this.nTr).removeClass('row_selected');
		});
		$(event.target.parentNode).addClass('row_selected');
                var anSelected = fnGetSelected( oTable );
                var aData = oTable.fnGetData(anSelected[0]); // get datarow
                
    if (anSelected!="")  // null if we clicked on title row
    {
    
                $('#exportType').val(aData[0]);
                }
	});
function fnGetSelected( oTableLocal )
{
	var aReturn = new Array();
	var aTrs = oTableLocal.fnGetNodes();
	
	for ( var i=0 ; i<aTrs.length ; i++ )
	{
		if ( $(aTrs[i]).hasClass('row_selected') )
		{
			aReturn.push( aTrs[i] );
		}
	}
	return aReturn;
}
$('button[id^=edit]').click( function() {
    var anSelected = fnGetSelected( oTable );
    var aData = oTable.fnGetData(anSelected[0]); // get datarow
    if (anSelected!="")  // null if we clicked on title row
    {
        console.log(anSelected);
        //becouse all the data can be reordered and hidden any additinal info like ID needs to be stored in <tr> and accesed by anSelected[0].id where .id is data needet
        $.colorbox({
            href:"{$_subdomain}/LookupTables/processJobFaultCodesLookups/id="+anSelected[0].id,
            title: "Edit JobFaultCodesLookups",
            opacity: 0.75,
            width:800,
            overlayClose: false,
            escKey: false
        });
    }else{
        alert("Please select row first");
    }
} );
$('button[id^=delete]').click( function() {
    var anSelected = fnGetSelected( oTable );
    var aData = oTable.fnGetData(anSelected[0]); // get datarow
    if (anSelected!="")  // null if we clicked on title row
    {
        //becouse all the data can be reordered and hidden any additinal info like ID needs to be stored in <tr> and accesed by anSelected[0].id where .id is data needet
        if (confirm('Are you sure you want to delete this entry from database?')) {
            window.location="{$_subdomain}/LookupTables/deleteJobFaultCodesLookups/id="+anSelected[0].id
        } else {
        // Do nothing!
        }
    }else{
        alert("Please select row first");
    }
} );
$("#JobFaultCodesLookupsResults  tbody").dblclick(function(event) {
    $(oTable.fnSettings().aoData).each(function (){
        $(this.nTr).removeClass('row_selected');
    });
    $(event.target.parentNode).addClass('row_selected');
    var anSelected = fnGetSelected( oTable );
    var aData = oTable.fnGetData(anSelected[0]); // get datarow
    if (anSelected!="")  // null if we clicked on title row
    {
        $.colorbox({ 
            href:"{$_subdomain}/LookupTables/processJobFaultCodesLookups/id="+anSelected[0].id,
            title: "Edit Job Fault Codes",
            opacity: 0.75,
            width:800,
            overlayClose: false,
            escKey: false
        });
    }else{
        alert("Please select row first");
    }
} );  
$('#serviceProviderSelect').change( function() {
    if($(this).val()!="0"){
        $('#tt-Loader').show();
        $('#JobFaultCodesLookupsResults').hide();
        oTable.fnReloadAjax("{$_subdomain}/LookupTables/loadJobFaultCodesLookupsTable/serviceProviderID="+$(this).val()+"/");
    }else{
        $('#tt-Loader').show();
        $('#JobFaultCodesLookupsResults').hide();
        oTable.fnReloadAjax("{$_subdomain}/LookupTables/loadJobFaultCodesLookupsTable/");
    }
} );  
$('input[id^=inactivetick]').click( function() {
    $('#unaprovedtick').attr("checked",false);
    if($(this).attr("checked")=="checked"){
        $('#tt-Loader').show();
        $('#SuppliersResults').hide();
        oTable.fnReloadAjax("{$_subdomain}/LookupTables/loadJobFaultCodesLookupsTable/inactive=1/");
    }else{
        $('#tt-Loader').show();
        $('#SuppliersResults').hide();
        oTable.fnReloadAjax("{$_subdomain}/LookupTables/loadJobFaultCodesLookupsTable/");
    }
} );*/

{* Updated By Praveen Kumar N : Jquery Inactive Function to send the inactive status [START] *}
$('input[id^=inactivetick]').click( function() {
      
		    if ($('#inactivetick').is(':checked')) {
			
			    window.location="{$_subdomain}/LookupTables/jobFaultCodesLookups/"+urlencode("{$sId}")+'/'+"Inactive";
		     } else {
			    window.location="{$_subdomain}/LookupTables/jobFaultCodesLookups/";
		     }		
                 
	} ); 
	
	$('input[id^=unaprovedtick]').click( function() {
      
     
		     if ($('#unaprovedtick').is(':checked')) {
			window.location="{$_subdomain}/LookupTables/jobFaultCodesLookups/"+urlencode("{$sId}")+'/'+"Pending";
		     } else {
			window.location="{$_subdomain}/LookupTables/jobFaultCodesLookups/";
		     }                    
		
                 
	} );  
	
{* [END] *}
    
});//doc ready end
function JobFaultCodesLookupsInsert()
{
    $.colorbox({
        href:"{$_subdomain}/LookupTables/processJobFaultCodesLookups/",
        title: "Insert Job Fault Codes",
        opacity: 0.75,
        width:800,
        overlayClose: false,
        escKey: false
    });
}
function JobFaultCodesLookupsEdit()
{
    var anSelected = fnGetSelected( oTable );
    var aData = oTable.fnGetData(anSelected[0]);
    $.colorbox({
        href:"{$_subdomain}/LookupTables/processJobFaultCodesLookups/id="+aData,
        title: "Edit Job Fault Codes",
        opacity: 0.75,
        width:800,
        overlayClose: false,
        escKey: false
    });
}        
function filterTable(name){
    $('input[type="text"]','#JobFaultCodesLookupsResults_filter').val(name);
    e = jQuery.Event("keyup");
    e.which = 13;
    $('input[type="text"]','#JobFaultCodesLookupsResults_filter').trigger(e);
}
</script>
   
{/block}

{block name=body}
<div style="float:right">
    <a href="#" onclick="showTablePreferences();">Display Preferences</a>
</div>
<div class="breadcrumb" style="width:100%">
    <div>
        <a href="{$_subdomain}/SystemAdmin" >{$page['Text']['system_admin_home_page']|escape:'html'}</a> / <a href="{$_subdomain}/SystemAdmin/index/lookupTables" >{$page['Text']['lookup_tables']|escape:'html'}</a> / {$page['Text']['job_fault_codes_lookups']|escape:'html'}
    </div>
</div>
<div class="main" id="home" style="width:100%">
    <div class="ServiceAdminTopPanel" >
        <fieldset>
            <legend title="" >{$page['Text']['job_fault_codes_lookups']|escape:'html'}</legend>
            <p><label>{$page['Text']['job_fault_codes_lookups_description']|escape:'html'}</label></p>
        </fieldset>
    </div>
        <br/> {* added by thirumal *}
    <div class="ServiceAdminResultsPanel" id="JobFaultCodesLookupsResultsPanel" >
        <form id="nIdForm" class="nidCorrections">
            {if isset($splist)}
            {$page['Labels']['serviceProvider_label']|escape:'html'}
            <select id="serviceProviderFilter" name="serviceProviderFilter">
                <option selected="selected">All Service Providers</option>
                {foreach $splist as $s}
                    <option value="{$s.ServiceProviderID}" {if $sId eq $s.ServiceProviderID}selected="selected"{/if}>{$s.Acronym}</option>
                {/foreach}
            </select>
            {/if}
        </form>
        <table id="JobFaultCodesLookupsResults" border="0" cellpadding="0" cellspacing="0" class="browse">
            <thead>
                <tr>
                    <th title="{$page['Text']['jobfaultcode_lookupid']|escape:'html'}">{$page['Text']['jobfaultcode_lookupid']|escape:'html'}</th>
                    <th title="{$page['Text']['jobfaultcode_lookupname']|escape:'html'}">{$page['Text']['jobfaultcode_lookupname']|escape:'html'}</th>
                    <th title="{$page['Text']['description']|escape:'html'}">{$page['Text']['description']|escape:'html'}</th>
                    <th title="{$page['Text']['manufacturer']|escape:'html'}">{$page['Text']['manufacturer']|escape:'html'}</th>
                    <th title="{$page['Text']['excludefrom_bouncertable']|escape:'html'}">{$page['Text']['excludefrom_bouncertable']|escape:'html'}</th>
                    <th title="{$page['Text']['forcepart_faultcode']|escape:'html'}">{$page['Text']['forcepart_faultcode']|escape:'html'}</th>
                    <th title="{$page['Text']['jobtype_availability']|escape:'html'}">{$page['Text']['jobtype_availability']|escape:'html'}</th>
                    <th title="{$page['Text']['promptfor_exchange']|escape:'html'}">{$page['Text']['promptfor_exchange']|escape:'html'}</th>
                    <th title="{$page['Text']['status']|escape:'html'}">{$page['Text']['status']|escape:'html'}</th>
                   <th align="center"> &nbsp; <input title="" type='checkbox' value=0 id='check_all' /> </th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
		{* Updated By Praveen Kumar N : Show Inactive Tick Box to display Inactive / Unapproved Records   [START] *}
		<div style="width:100%;text-align: right"><input id="inactivetick"  type="checkbox"  value="Inactive" {if $dataStatus eq 'Inactive'} checked=checked {/if} > Show Inactive&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>
     {if $SupderAdmin=="1"} <input id="unaprovedtick"  type="checkbox"  {if $dataStatus eq 'Pending'}checked=checked{/if} > Show Unapproved {/if}</div>
		{* [END] *}
    <hr>
    <button class="gplus-blue" style="float:right" type="button" onclick="window.location='{$_subdomain}/SystemAdmin/index/lookupTables'">{$page['Buttons']['finish_btn']|escape:'html'}</button>
</div>
{/block}