<head>
    <script>
        $(document).ready(function() { 
            $('.helpTextIconQtip').each(function() { 
                $HelpTextCode =  $(this).attr("id");
                // We make use of the .each() loop to gain access to each element via the "this" keyword...
                $(this).qtip({ 
                    hide: 'unfocus',
                    events: { 
                        hide: function(){ 
                            $(this).qtip('api').set('content.text', '<img src="{$_subdomain}/images/ajax-loader.gif" '); // Direct API method  
                        }
                    },
                    content: { 
                        // Set the text to an image HTML string with the correct src URL to the loading image you want to use
                        text: '<img src="{$_subdomain}/images/ajax-loader.gif" >',
                        ajax: { 
                            url: '{$_subdomain}/Popup/helpText/' + urlencode($HelpTextCode) + '/Qtip=1/' + Math.random(),
                            once: false // Re-fetch the content each time I'm shown 
                        },
                        title: { 
                            text: 'Help', // Give the tooltip a title using each elements text
                            button: true
                        }
                    },
                    position: { 
                        at: 'bottom center', // Position the tooltip above the link
                        my: 'top center',
                        viewport: $(window), // Keep the tooltip on-screen at all times
                        effect: true // Disable positioning animation
                    },
                    show: { 
                        event: 'click',
                        solo: true // Only show one tooltip at a time
                    },
                    style: { 
                        classes: 'qtip-tipped  qtip-shadow'
                    }
                }) 
            }) 
        });

        function saSetup() { 
            $('#saSetup').toggle();
            $('#userDisplayPrefHelp').toggle();
            $('#saDisplayPrefHelp').toggle();
            
            $('#userSetup').toggle();
            $.colorbox.resize();
        }
        
        function setOldValue(v)  {
            $("#oldvalue").val(v);
        }
        
        function changeNewValue(v,i) { 
            $('input[value='+v+']').each(function(u) { 
                //alert($(u).attr("id"));
                // alert(i); 
                if($(u).attr("id")!=i) { 
                    $('input[value='+v+']').val($("#oldvalue").val());
                }
            });
        }
        
        $(function() { 
            $("#sortable tbody").sortable({ 
                handle: 'td',axis:'y',
                update: function(ev,ui) { 
                    $('table tr:visible').each(function(){ 
                        $(this).children('td:last-child').children('input').val($(this).index()+1);
                    }) 
                },
                placeholder: 'group_move_placeholder'
            })
            
            $("#sortableSA tbody").sortable({ 
                handle: 'td',axis:'y',
                update: function(ev,ui) { 
                    $('table tr').each(function(){ 
                        $(this).children('td:last-child').children('input').val($(this).index()+1);
                    }) 
                },
                forcePlaceholderSize: true,
                placeholder: 'group_move_placeholder'
            }).disableSelection();
            
            //$('#sortable').dataTable(
            // {
            //"sDom":"",
            // "aaSorting":  [[4,'asc']],
            // "bPaginate": false
            //}
            //);
        });
        
        function filterCheckedVisibilitySA() { 
            $('#sortableSA tr td input:checkbox.vissa').each((function (){ 
                //console.log(this);
                if($('#savisch').attr("checked")=="checked") { 
                    if($(this).attr("checked")!="checked") { 
                        $(this).parent().parent().hide();
                    } 
                }
                else { 
                    $(this).parent().parent().show();
                }
            }));
        }
        
        function filterCheckedVisibility() { 
            $('#sortable tr td input:checkbox.vis').each((function () { 
                //console.log(this);
                if($('#visch').attr("checked")=="checked") { 
                    if($(this).attr("checked")!="checked") { 
                        $(this).parent().parent().hide();
                    }
                }
                else { 
                    if($(this).parent().parent().attr('class')!='alwayshid') { 
                        $(this).parent().parent().show(); 
                    }
                } 
            }));
        }
        
        function filterCheckedActiveSA() { 
            $('#sortableSA tr td input:checkbox.activesa').each((function (){ 
                //console.log(this);
                if($('#saactivech').attr("checked")=="checked"){ 
                    if($(this).attr("checked")!="checked"){ 
                        $(this).parent().parent().hide(); 
                    }
                }
                else { 
                    $(this).parent().parent().show();
                }                   
            }));
        }
        
        function resetUserSettings() { 
            if (confirm('All user data will be reset. Are you sure you wish to continue?')) { 
                window.location="{$_subdomain}/{$controller}/resetDisplayPreferences/typeAction={$typeAction}"
            } 
            else { 
            // Do nothing! 
            }
        }
    </script>
     <script type="text/javascript" src="{$_subdomain}/js/table.js"></script>
</head>
<body > 
    <div id=helpContainer></div>    
    <div id="popUpContainerMainDiv">
        <img style="float: left" src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="userDisplayPrefHelp" class="helpTextIconQtip"  title="Help" alt="help" width="15" height="15" >
        <img style="display:none"  src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="saDisplayPrefHelp" class="helpTextIconQtip"  title="Help" alt="help" width="15" height="15" >
        <input type=hidden id="oldvalue" name="oldvalue">
        <div id="userSetup" style="float:left;width:540px;">
           {if $SupderAdmin} <div style="float:right;position: relative;"><button type="button"  class="gplus-blue" onclick="saSetup();">Preference Manager</button></div>{/if}
            <br>
            <fieldset style="width:540px">
                <legend>User Table Display Preferences</legend>
                <form name="displayPreferences" method="POST" action="{$_subdomain}/{$controller}/saveDisplayPreferences/typeAction={$typeAction}">
                     <input type="hidden" name="numberOfRecords" value="{$data_keys|@count}"> 
                     <div style="float:right">
                         <a href="#" onclick="resetUserSettings()">Reset</a>
                     </div>
                     <div style="height:20px">&nbsp;</div>
                     <div>
                         <div style="float:right;position:relative;">Show Tagged Only<input id="visch" onclick="filterCheckedVisibility()" type="checkbox"> </div>
                     </div>
                     <table class=" table-sortable:date table-autosort:3" id="sortable" style="width:540px;cursor:pointer;">
                         <thead>
                             <th style="width:43%">DB Field Name</th>
                             <th  style="width:33%">User Specific Name</th>
                             <th style="width:10%">Displayed Columns</th>
                             <th></th>
                         </thead>
                         <tbody>
                             {for $er=0 to $data_keys|@count-1}
                                <tr {if (isset($columnStatusStringSA[$er]) && $columnStatusStringSA[$er]==1) || (isset($admin)&&$admin==1)}{$active="y"}{else}{$active="n"} style="display:none" class="alwayshid" {/if} >
                                    <td>
                                        {if isset($columnNameStringSA[$er])&&$columnNameStringSA[$er]!=""}{$columnNameStringSA[$er]}{else}{$data_keys[$er]}{/if}
                                    </td>
                                    <td>
                                        <input type="text" name="columnName_{$er}" value="{if isset($columnNameString[$er])}{$columnNameString[$er]}{/if}">
                                    </td>
                                    <td style="text-align: center">
                                        {if isset($columnDisplayString[$er])}{$vis=$columnDisplayString[$er]}{else}{if isset($columnDisplayStringSA[$er])}{$vis=$columnDisplayStringSA[$er]}{else}{$vis=0}{/if}{/if}
                                        <input class="vis" type="checkbox" name="vis_{$er}" value="{$vis}" {if $vis==1 &&$active=="y"}checked=checked{/if}> 
                                    </td>
                                    <td>
                                        <input readonly="readonly"  type="hidden" style="width:40px;"  id="columnOrder_{$er}" name="columnOrder_{$er}" value="{if isset($columnOrderString[$er])}{$columnOrderString[$er]+1}{elseif isset($columnOrderStringSA[$er])}{$columnOrderStringSA[$er]+1}{else}{$er+1}{/if}">
                                    </td>
                                 </tr>
                             {/for}
                         </tbody>
                     </table>
                     <button type="submit" class="gplus-blue" style="float:left">Save</button>
                     <button type="button" onclick="$.colorbox.close();" class="gplus-red" style="float:right">Cancel</button>
                </form>
            </fieldset>
        </div>
        <div id="saSetup" style="display:none;float:right">
            <div style="float:right;position: relative;">
                <button type="button" style="float:right" class="gplus-blue" onclick="saSetup();">User Setup</button>
            </div>
            <br>
            <fieldset style="width:540px">
                <legend>System Admin Table Display Preferences</legend>
                <form name="SAdisplayPreferences" method="POST" action="{$_subdomain}/{$controller}/saveDisplayPreferences/typeAction={$typeAction}">
                    <input type="hidden" name="numberOfRecords" value="{$data_keys|@count}">  
                    <input type="hidden" name="sa" value="yes">  
                    <table style="width:540px">
                        <tr>
                            <td style="width:85px"></td>
                            <td style="width:200px">Show Tagged Only<input id="saactivech" onclick="filterCheckedActiveSA()"  type="checkbox"></td>
                            <td  style="width:65px"></td>
                            <td style="width:200px">Show Tagged Only<input id="savisch" onclick="filterCheckedVisibilitySA()" type="checkbox"></td>
                        </tr>
                    </table>
                    <table class=" table-sortable:date table-autosort:4" id="sortableSA" style="cursor:pointer;width:540px">
                        <thead>
                            <th style="width:43%">DB Field Name</th>
                            <th  style="width:10%">Active</th>
                            <th  style="width:33%">Default Name</th>
                            <th  style="width:10%">Default Table View</th>
                            <th></th>
                        </thead>
                        <tbody>
                            {for $er=0 to $data_keys|@count-1}
                                <tr>
                                    <td>
                                        {$data_keys[$er]}
                                    </td>
                                    <td>
                                        {if isset($columnStatusStringSA[$er])}{$st=$columnStatusStringSA[$er]}{else}{$st=0}{/if}
                                        <input class="activesa" type="checkbox" name="st_{$er}" value="{$st}" {if $st==1}checked=checked{/if}> 
                                    </td>
                                    <td>
                                        <input  type="text" name="columnName_{$er}" value="{if isset($columnNameStringSA[$er])}{$columnNameStringSA[$er]}{/if}">
                                    </td>
                                    <td style="text-align: center">
                                        {if isset($columnDisplayStringSA[$er])}{$vis=$columnDisplayStringSA[$er]}{else}{$vis=0}{/if}
                                        <input class="vissa"  type="checkbox" name="vis_{$er}" value="{$vis}" {if $vis==1}checked=checked{/if}> 
                                    </td>
                                    <td style="text-align:center;">
                                        <input type="hidden" style="width:40px;" name="columnOrder_{$er}" value="{if isset($columnOrderStringSA[$er])}{$columnOrderStringSA[$er]+1}{else}{$er+1}{/if}">
                                    </td>
                                </tr>
                            {/for}
                        </tbody>
                    </table>
                    <button type="submit" class="gplus-blue">Save</button>
                    <button type="button" class="gplus-red">Cancel</button>
                </form>
            </fieldset>
        </div>
    </div>
</body>