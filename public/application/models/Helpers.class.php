<?php

require_once('CustomModel.class.php');

//This class is for abstract standalone helper functions, since this framework doesn't have helpers functionality
//2012-11-09 © Vic <v.rutkunas@pccsuk.com>

class Helpers extends CustomModel {
        

    
    public function __construct($controller) {
    
        parent::__construct($controller); 
        
    }

    
    
    //This function returns excel column names 
    //Based on zero indexed numbers, meaning 0 == A, 1 == B, etc
    //2012-11-09 © Vic v.rutkunas@pccsuk.com
    
    public function getExcelColName($colNum) {
	
	$numeric = $colNum % 26;
	$letter = chr(65 + $numeric);
	$num2 = intval($colNum / 26);
	if($num2 > 0) {
	    return $this->getExcelColName($num2 - 1) . $letter;
	} else {
	    return $letter;
	}
	
    }
    
    
    
    /**
     * Inserts a new key/value before the key in the array.
     *
     * @param $key
     *   The key to insert before.
     * @param $array
     *   An array to insert in to.
     * @param $new_key
     *   The key to insert.
     * @param $new_value
     *   An value to insert.
     *
     * @return
     *   The new array if the key exists, FALSE otherwise.
     *
     * @see array_insert_after()
     */
    
    public function array_insert_before($key, array &$array, $new_key, $new_value) {
	if(array_key_exists($key, $array)) {
	    $new = [];
	    foreach($array as $k => $value) {
		if($k === $key) {
		    $new[$new_key] = $new_value;
		}
		$new[$k] = $value;
	    }
	    return $new;
	}
	return false;
    }

    
    
    /**
     * Inserts a new key/value after the key in the array.
     *
     * @param $key
     *   The key to insert after.
     * @param $array
     *   An array to insert in to.
     * @param $new_key
     *   The key to insert.
     * @param $new_value
     *   An value to insert.
     *
     * @return
     *   The new array if the key exists, FALSE otherwise.
     *
     * @see array_insert_before()
     */
    
    public function array_insert_after($key, array &$array, $new_key, $new_value) {
	if(array_key_exists($key, $array)) {
	    $new = [];
	    foreach($array as $k => $value) {
		$new[$k] = $value;
		if($k === $key) {
		    $new[$new_key] = $new_value;
		}
	    }
	    return $new;
	}
	return false;
    }    
    
    
}

?>
