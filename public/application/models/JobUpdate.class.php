<?php

/**
 * JobUpdate.class.php
 * 
 * Routines for job update screen.
 *
 * @author     Vykintas Rutkunas <v.rutkunas@pccsuk.com>
 * @copyright  2013 PC Control Systems
 * @link       http://www.pccontrolsystems.com
 * @version    1.01
 *   
 * Changes
 * Date        Version Author                Reason
 * ??/01/2013  1.00    Vykintas Rutkunas     Initial Version
 * 17/05/2013  1.01    Andrew J. Williams    Updates to Contact History to take into account Source Field (Contact History Vs Remote Engineer)
 ******************************************************************************/

require_once('CustomModel.class.php');
require_once('TableFactory.class.php');
require_once('Constants.class.php');
require_once('Functions.class.php');

class JobUpdate extends CustomModel {

    private $conn;
    private $mysqli;
    public $config;
    
    
    public function __construct($controller) {
    
        parent::__construct($controller); 

        $this->conn = $this->Connect( 
	    $this->controller->config['DataBase']['Conn'],
	    $this->controller->config['DataBase']['Username'],
	    $this->controller->config['DataBase']['Password'] 
	);  
        
        //Initialize MySQLi connection
        $dbArr = explode("=",$this->controller->config['DataBase']['Conn']);
        $dbName = $dbArr[2];
        $dbHostArr = explode(";",$dbArr[1]);
        $dbHost = $dbHostArr[0];
        $this->mysqli = new mysqli( 
	    $dbHost,
	    $this->controller->config['DataBase']['Username'],
	    $this->controller->config['DataBase']['Password'],
	    $dbName
	);
	
	$this->config = $this->controller->readConfig('application.ini');
  
    }

    
    
    public function getConditionCodes($job) {
	
	$q = "	SELECT	    DISTINCT(cc.ConditionCode), cc.ConditionCodeID, cc.ConditionCodeDescription
		FROM	    condition_code AS cc
		LEFT JOIN   condition_code_subset AS sub ON cc.ConditionCodeID = sub.ConditionCodeID
		WHERE	    CASE
				WHEN	(
					    SELECT  COUNT(*) 
					    FROM    condition_code_subset AS ccs 
					    WHERE   ccs.ManufacturerID = :manufacturerID AND RepairSkillID = :repairSkillID
					) > 0
				THEN	
					CASE
					    WHEN :repairSkillID IS NOT NULL AND :repairSkillID != ''
					    THEN ManufacturerID = :manufacturerID AND RepairSkillID = :repairSkillID
					    ELSE ManufacturerID = :manufacturerID
					END
				ELSE	TRUE
			    END";
	
	$values = ["manufacturerID" => $job["ManufacturerID"], "repairSkillID" => $job["RepairSkillID"]];
	
	$result = $this->query($this->conn, $q, $values);
	
	return $result;
	
    }
    
    
      public function getPartSectionCodes($job) {
	
	$q = "	SELECT	    cc.PartSectionCode, cc.PartSectionCodeID, cc.PartSectionCodeDescription
		FROM	    part_section_code AS cc
		LEFT JOIN   part_section_code_subset AS sub ON cc.PartSectionCodeID = sub.PartSectionCodeID
		WHERE	    CASE
				WHEN	(
					    SELECT  COUNT(*) 
					    FROM    part_section_code_subset AS ccs 
					    WHERE   ccs.ManufacturerID = :manufacturerID AND RepairSkillID = :repairSkillID
					) > 0
				THEN	sub.ManufacturerID = :manufacturerID AND sub.RepairSkillID = :repairSkillID
				ELSE	TRUE
			    END";
	
	$values = ["manufacturerID" => $job["ManufacturerID"], "repairSkillID" => $job["RepairSkillID"]];
	
	$result = $this->query($this->conn, $q, $values);
	
	return $result;
	
    }
    
      public function getPartDefectCodes($job) {
	
	$q = "	SELECT	    cc.PartDefectCode, cc.PartDefectCodeID, cc.PartDefectCodeDescription
		FROM	    part_defect_code AS cc
		LEFT JOIN   part_defect_code_subset AS sub ON cc.PartDefectCodeID = sub.PartDefectCodeID
		WHERE	    CASE
				WHEN	(
					    SELECT  COUNT(*) 
					    FROM    part_defect_code_subset AS ccs 
					    WHERE   ccs.ManufacturerID = :manufacturerID AND RepairSkillID = :repairSkillID
					) > 0
				THEN	sub.ManufacturerID = :manufacturerID AND sub.RepairSkillID = :repairSkillID
				ELSE	TRUE
			    END";
	
	$values = ["manufacturerID" => $job["ManufacturerID"], "repairSkillID" => $job["RepairSkillID"]];
	
	$result = $this->query($this->conn, $q, $values);
	
	return $result;
	
    }
      public function getPartRepairCodes($job) {
	
	$q = "	SELECT	    cc.PartRepairCode, cc.PartRepairCodeID, cc.PartRepairCodeDescription
		FROM	    part_repair_code AS cc
		LEFT JOIN   part_repair_code_subset AS sub ON cc.PartRepairCodeID = sub.PartRepairCodeID
		WHERE	    CASE
				WHEN	(
					    SELECT  COUNT(*) 
					    FROM    part_repair_code_subset AS ccs 
					    WHERE   ccs.ManufacturerID = :manufacturerID AND RepairSkillID = :repairSkillID
					) > 0
				THEN	sub.ManufacturerID = :manufacturerID AND sub.RepairSkillID = :repairSkillID
				ELSE	TRUE
			    END";
	
	$values = ["manufacturerID" => $job["ManufacturerID"], "repairSkillID" => $job["RepairSkillID"]];
	
	$result = $this->query($this->conn, $q, $values);
	
	return $result;
	
    }

    
    
    public function getSymptomCodes($job, $symptomConditionID = null, $symptomTypeID = null) {
	
	$q = "	SELECT	    DISTINCT(sc.SymptomCode), sc.SymptomCodeID, sc.SymptomCodeDescription 
		FROM	    symptom_code AS sc
		LEFT JOIN   symptom_code_subset AS sub ON sc.SymptomCodeID = sub.SymptomCodeID
		WHERE	    CASE
				WHEN	(
					    SELECT  COUNT(*) 
					    FROM    symptom_code_subset AS scs 
					    WHERE   scs.ManufacturerID = :manufacturerID AND RepairSkillID = :repairSkillID
					) > 0
				THEN	
					CASE
					    WHEN :repairSkillID IS NOT NULL AND :repairSkillID != ''
					    THEN ManufacturerID = :manufacturerID AND RepairSkillID = :repairSkillID
					    ELSE ManufacturerID = :manufacturerID
					END
				ELSE	TRUE
			    END
			    
			    AND
			    
			    CASE
				WHEN	:symptomConditionID IS NOT NULL AND :symptomConditionID != ''
				THEN	sub.SymptomConditionID = :symptomConditionID
				ELSE	TRUE
			    END
			    
			    AND
			    
			    CASE
				WHEN	:symptomTypeID IS NOT NULL AND :symptomTypeID != ''
				THEN	sub.symptomTypeID = :symptomTypeID
				ELSE	TRUE
			    END
	     ";
	
	$values = [
	    "manufacturerID" => $job["ManufacturerID"], 
	    "repairSkillID" => $job["RepairSkillID"],
	    "symptomConditionID" => $symptomConditionID,
	    "symptomTypeID" => $symptomTypeID
	];
	
	$result = $this->query($this->conn, $q, $values);
	
	return $result;
	
    }
    
    
    
    public function getSymptomTypes($job) {
	
	$q = "	SELECT	SymptomTypeID, 
			SymptomTypeName
		FROM	symptom_type
		WHERE	CASE
			    WHEN :repairSkillID IS NOT NULL AND :repairSkillID != ''
			    THEN ManufacturerID = :manufacturerID AND RepairSkillID = :repairSkillID
			    ELSE ManufacturerID = :manufacturerID
			END
	     ";
	
	$values = ["manufacturerID" => $job["ManufacturerID"], "repairSkillID" => $job["RepairSkillID"]];
	
	$result = $this->query($this->conn, $q, $values);
	
	return $result;
	
    }
    
    
    
    public function getSymptomConditions($job) {
	
	$q = "	SELECT	SymptomConditionID, 
			SymptomConditionName
		FROM	symptom_condition
		WHERE	CASE
			    WHEN :repairSkillID IS NOT NULL AND :repairSkillID != ''
			    THEN ManufacturerID = :manufacturerID AND RepairSkillID = :repairSkillID
			    ELSE ManufacturerID = :manufacturerID
			END
	     ";
	
	$values = ["manufacturerID" => $job["ManufacturerID"], "repairSkillID" => $job["RepairSkillID"]];
	
	$result = $this->query($this->conn, $q, $values);
	
	return $result;
	
    }

    
    
    public function getServiceCategory($jobID) {
	
	$q = "SELECT ServiceCategory FROM job WHERE JobID = :jobID";
	$values = ["jobID" => $jobID];
	$result = $this->query($this->conn, $q, $values);
	return (isset($result[0]["ServiceCategory"]) ? $result[0]["ServiceCategory"] : null);
	
    }
    
    
    
    public function getQAQuestions() {
	
	$q = "SELECT * FROM quality_assurance_questions WHERE BrandID = :brandID";
	$values = ["brandID" => $this->controller->user->DefaultBrandID];
	$result = $this->query($this->conn, $q, $values);
	return $result;
	
    }
    
    
    
    public function getQAQuestionData($jobID) {
	
	$q = "SELECT QAQuestionID FROM quality_assurance_data WHERE JobID = :jobID";
	$values = ["jobID" => $jobID];
	$result = $this->query($this->conn, $q, $values);
	$return = [];
	foreach($result as $el) {
	    $return[] = $el["QAQuestionID"];
	}
	return $return;
	
    }
    
    
    
    public function updateQAQuestions($json) {
	
	$data = json_decode($json);
	foreach($data as $question) {
	    if($question->id != "") {
		$q = "	UPDATE	quality_assurance_questions
			SET	QuestionText = :text
			WHERE	QAQuestionID = :id
		     ";
		$values = ["text" => $question->text, "id" => $question->id];
		$result = $this->execute($this->conn, $q, $values);
	    } else {
		$q = "	INSERT INTO quality_assurance_questions
				    (
					BrandID,
					QuestionText,
					ModifiedUserID
				    )
			VALUES
				    (
					:brandID,
					:text,
					:userID
				    )
		     ";
		$values = [
		    "brandID" => $this->controller->user->DefaultBrandID,
		    "text" => $question->text,
		    "userID" => $this->controller->user->UserID
		];
		$result = $this->execute($this->conn, $q, $values);
	    }
	}	
	
    }
    
    
    
    public function getWarrantyServiceTypes($job) {
	
	$q = "	SELECT	    wst.Code, wst.WarrantyServiceTypeID, wst.Description
		FROM	    warranty_service_type AS wst
		LEFT JOIN   warranty_service_type_subset AS sub ON wst.WarrantyServiceTypeID = sub.WarrantyServiceTypeID
		WHERE	    CASE
				WHEN	(
					    SELECT  COUNT(*) 
					    FROM    warranty_service_type_subset AS wsts 
					    WHERE   wsts.ManufacturerID = :manufacturerID AND wsts.RepairSkillID = :repairSkillID
					) > 0
				THEN	
					CASE
					    WHEN :repairSkillID IS NOT NULL AND :repairSkillID != ''
					    THEN ManufacturerID = :manufacturerID AND RepairSkillID = :repairSkillID
					    ELSE ManufacturerID = :manufacturerID
					END
				ELSE	TRUE
			    END";
	
	$values = ["manufacturerID" => $job["ManufacturerID"], "repairSkillID" => $job["RepairSkillID"]];
	
	$result = $this->query($this->conn, $q, $values);
	
	return $result;
	
    }

    
    
    public function getWarrantyDefectTypes($job) {
	
	$q = "	SELECT	    wdt.Code, wdt.WarrantyDefectTypeID, wdt.Description
		FROM	    warranty_defect_type AS wdt
		LEFT JOIN   warranty_defect_type_subset AS sub ON wdt.WarrantyDefectTypeID = sub.WarrantyDefectTypeID
		WHERE	    CASE
				WHEN	(
					    SELECT  COUNT(*) 
					    FROM    warranty_defect_type_subset AS wdts 
					    WHERE   wdts.ManufacturerID = :manufacturerID AND wdts.RepairSkillID = :repairSkillID
					) > 0
				THEN	
					CASE
					    WHEN :repairSkillID IS NOT NULL AND :repairSkillID != ''
					    THEN ManufacturerID = :manufacturerID AND RepairSkillID = :repairSkillID
					    ELSE ManufacturerID = :manufacturerID
					END
				ELSE	TRUE
			    END";
	
	$values = ["manufacturerID" => $job["ManufacturerID"], "repairSkillID" => $job["RepairSkillID"]];
	
	$result = $this->query($this->conn, $q, $values);
	
	return $result;
	
    }
  
    
    
    public function getCompletionStatuses($data) {
	
	$q = "	SELECT	CompletionStatusID,
			CompletionStatus,
			Description
		FROM	completion_status
		WHERE	JobCategory = :jobCategory AND
			CASE
			    WHEN :serviceCategory = '' OR :serviceCategory IS NULL
			    THEN TRUE
			    ELSE ServiceCategory = :serviceCategory
			END
	     ";
	
	if(isset($data["jobCategory"]) && isset($data["serviceCategory"])) {
	    $values = ["jobCategory" => $data["jobCategory"], "serviceCategory" => $data["serviceCategory"]];
	} else {
	    $values = ["jobCategory" => $data["JobCategory"], "serviceCategory" => $data["ServiceCategory"]];
	}
	
	$result = $this->query($this->conn, $q, $values);
	
	return $result;
	
    }
    
    
    
    public function getServiceAction($data) {
	
	if($data["ServiceAction"] == null) {
	    return null;
	}
	
	//$obj = json_decode($data["ServiceAction"]);
	
	$action = null;
	
	switch ($data["ServiceAction"]) {
	    case "software_update": {
		$action = "SoftwareUpdate";
		break;
	    }
	    case "education": {
		$action = "CustomerEducation";
		break;
	    }
	    case "setup": {
		$action = "SettingsSetup";
		break;
	    }
	    case "repair_required": {
		$action = "RepairRequired";
		break;
	    }
	    case "damaged": {
		$action = "DamagedBeyondRepair";
		break;
	    }
	    case "faulty_acc": {
		$action = "FaultyAccessory";
		break;
	    }
	    case "non_uk_mod": {
		$action = "NonUKModel";
		break;
	    }
	    case "unresolvable": {
		$action = "Unresolvable";
		break;
	    }
	    case "branch_repair": {
		$action = "BranchRepair";
		break;
	    }
	}
	
	/*
	foreach($obj as $key => $val) {
	    if($val) {
		return $key;
	    }
	}
	*/
	
	return $action;
	
    }
    
    
    
    public function getCompletionStatusExplanation($data) {
	
	if($data["CompletionStatusID"] == null) {
	    return null;
	}
	
	$q = "SELECT Description FROM completion_status WHERE CompletionStatusID = :completionStatusID";
	$values = ["completionStatusID" => $data["CompletionStatusID"]];
	$result = $this->query($this->conn, $q, $values);
	
	return (isset($result[0]["Description"]) ? $result[0]["Description"] : null);
	
    }
    
  
    
    public function saveServiceActionTracking($data) {
	
	/*
	$serviceActions = [];
	$serviceActions["SoftwareUpdate"] = isset($data["ServiceAction"]) && $data["ServiceAction"] == "SoftwareUpdate";
	$serviceActions["Education"] = isset($data["ServiceAction"]) && $data["ServiceAction"] == "CustomerEducation";
	$serviceActions["Settings"] = isset($data["ServiceAction"]) && $data["ServiceAction"] == "SettingsSetup";
	$serviceActions["RepairRequired"] = isset($data["ServiceAction"]) && $data["ServiceAction"] == "RepairRequired";
	$serviceActions["DamagedBeyondRepair"] = isset($data["ServiceAction"]) && $data["ServiceAction"] == "DamagedBeyondRepair";
	$serviceActions["FaultyAccessory"] = isset($data["ServiceAction"]) && $data["ServiceAction"] == "FaultyAccessory";
	$serviceActions["NonUKModel"] = isset($data["ServiceAction"]) && $data["ServiceAction"] == "NonUKModel";
	$serviceActions["Unresolvable"] = isset($data["ServiceAction"]) && $data["ServiceAction"] == "Unresolvable";
	$serviceActions["BranchRepair"] = isset($data["ServiceAction"]) && $data["ServiceAction"] == "BranchRepair";
	*/
	        
	$serviceAction = null;
	
	switch ($data["ServiceAction"]) {
	    case "SoftwareUpdate" : {
		$serviceAction = "software_update";
		break;
	    }
	    case "CustomerEducation" : {
		$serviceAction = "education";
		break;
	    }
	    case "SettingsSetup" : {
		$serviceAction = "setup";
		break;
	    }
	    case "RepairRequired" : {
		$serviceAction = "repair_required";
		break;
	    }
	    case "DamagedBeyondRepair" : {
		$serviceAction = "damaged";
		break;
	    }
	    case "FaultyAccessory" : {
		$serviceAction = "faulty_acc";
		break;
	    }
	    case "NonUKModel" : {
		$serviceAction = "non_uk_mod";
		break;
	    }
	    case "Unresolvable" : {
		$serviceAction = "unresolvable";
		break;
	    }
	    case "BranchRepair" : {
		$serviceAction = "branch_repair";
		break;
	    }
	    default: {
		$serviceAction = null;
		break;
	    }
	}
	//inserted record in customer table and get the customerid done by srinivas
	$q =  "	INSERT INTO customer
			    (   CustomerTitleID,
                                ContactFirstName,
                                ContactLastName,
                                ContactMobile,
                                ContactEmail,
                                DataProtection
                             )
		VALUES      
                            (    
                                :CustomerTitleID,
                                :ContactFirstName,
                                :ContactLastName,
                                :ContactMobile,
                                :ContactEmail,
                                :DataProtection
                                )
	     ";
        $values = [	   
            "CustomerTitleID"   =>  $data["CustomerTitleID"],
            "ContactFirstName"  =>  $data["ContactFirstName"],
            "ContactLastName"   =>  $data["ContactLastName"],
            "ContactMobile"     =>  $data["ContactMobile"],
            "ContactEmail"      =>  $data["ContactEmail"],
            "DataProtection"      =>  isset( $data["ContactAllow"]) ?  $data["ContactAllow"] : "No"
	];   
        
	$result = $this->execute($this->conn, $q, $values); 
        $CustomerID=$this->conn->lastInsertId();  
        //end of gettign cutomer id 
	$q = "	INSERT INTO job
			    (
				NetworkID,
				ClientID,
				BrandID,
				BranchID,
                                CustomerID,
				ManufacturerID,
				JobTypeID,
				ServiceTypeID,
				StatusID,
				ServiceBaseModel,
				ModelID,
				ServiceAction,
				ImeiNo,
				ReportedFault,
				DateOfPurchase,
				BookedBy,
				ModifiedUserID,              
				ModifiedDate,
				DateBooked,
				TimeBooked,
				ClosedDate,
				ServiceProviderDespatchDate,
				RepairCompleteDate
			    )
		VALUES
			    (
				:networkID,
				:clientID,
				:brandID,
				:branchID,
                                :CustomerID,
				106,
				2,
				:serviceTypeID,
				25,
				:modelNumber,
				CASE
				    WHEN (@modelID := (SELECT model.ModelID FROM model WHERE model.ModelNumber = :modelNumber))
				    THEN @modelID
				    ELSE NULL
				END,
				:serviceAction,
				:imeiNo,
				:reportedFault,
				:purchaseDate,
				:userID,
				:userID,                  
				NOW(),
				NOW(),
				NOW(),
				NOW(),
				NOW(),
				NOW()
			    )
	     ";
	
	$values = [
	    "networkID" => isset($this->controller->user->NetworkID) ? $this->controller->user->NetworkID : null,
	    "clientID" => isset($this->controller->user->ClientID) ? $this->controller->user->ClientID : null,
	    "brandID" => isset($this->controller->user->DefaultBrandID) ? $this->controller->user->DefaultBrandID : null,
	    "branchID" => isset($this->controller->user->BranchID) ? $this->controller->user->BranchID : null,
	    "modelNumber" => $data["modelNo"],
	    "serviceAction" => $serviceAction,
	    "imeiNo" => $data["imeiNo"],
	    "serviceTypeID" => $data["serviceType"],
	    "reportedFault" => $data["reportedFault"],
	    "purchaseDate" => (isset($data["purchaseDate"]) && $data["purchaseDate"] != "") ? $data["purchaseDate"] : null,
	    "userID" => $this->controller->user->UserID,
            "CustomerID"   =>  $CustomerID            
	];
        
	$result = $this->execute($this->conn, $q, $values); 	
    }
    
    
    
    public function getRemoteEngineerHistory($jobID) {
        
	/*if (defined('SUB_DOMAIN')) {
	    $subdomain = SUB_DOMAIN;
	} else {
	    $subdomain = "";
	}*/

	$q = "	SELECT	    DATE_FORMAT(ch.ContactDate, '%d/%m/%Y')	AS '0',
			    DATE_FORMAT(ch.ContactTime, '%H:%i')	AS '1',
			    ch.UserCode					AS '2',
			    IF (LEFT(ca.Action,12) = 'REMOTE ENG: ',SUBSTR(ca.Action,13),ca.Action) AS '3',
			    CASE
				WHEN	ch.Note = ca.Action
				THEN	NULL
				ELSE	ch.Note
			    END						AS '4',
			    CASE
				WHEN	ch.ImageURL IS NOT NULL AND ch.ThumbnailURL IS NOT NULL and sp.IPAddress IS NOT NULL
				THEN	CONCAT(sp.IPAddress,':',sp.Port,ch.ImageURL)
				ELSE	NULL
			    END						AS '5'
			
		FROM	    contact_history AS ch
		
		LEFT JOIN   contact_history_action AS ca ON ch.ContactHistoryActionID = ca.ContactHistoryActionID
                LEFT JOIN   job on job.JobID=:jobID
                LEFT JOIN   service_provider as sp on sp.ServiceProviderID=job.ServiceProviderID 
		
		WHERE	    ch.JobID = :jobID AND ca.Source = 'Remote Engineer'
		
		ORDER BY    ch.ContactDate, ch.ContactTime ASC
	     ";
	
	$values = ["jobID" => $jobID];
	
	$result = $this->query($this->conn, $q, $values);
	
	return $result;
	
    }
    
  
    
    public function getInboundFaults($job) {
	
	$q = "SELECT InboundFaultCodeID, Code FROM inbound_fault_code WHERE NetworkID = :networkID";
	$values = ["networkID" => $job["NetworkID"]];
	$result = $this->query($this->conn, $q, $values);
	return $result;
	
    }
    
    
    
    public function getAuthTypeAllowed($data) {
	
	$q = "	SELECT	CASE
			    WHEN    (SELECT COUNT(*) FROM ra_history WHERE JobID = :jobID) = 0
			    THEN    TRUE
			    
			    WHEN    (
					SELECT	    auth_types.AuthorisationTypeName
					FROM	    ra_history AS rh
					LEFT JOIN   ra_status AS rs ON rs.RAStatusID = rh.RAStatusID
					LEFT JOIN   authorisation_types AS auth_types ON auth_types.AuthorisationTypeID = rs.AuthorisationTypeID
					WHERE	    JobID = :jobID
					ORDER BY    rh.ModifiedDate DESC
					LIMIT	    1
				    ) LIKE :type
			    THEN    TRUE
			
			    WHEN    (
					SELECT	    rs.Final
					FROM	    ra_history AS rh
					LEFT JOIN   ra_status AS rs ON rs.RAStatusID = rh.RAStatusID
					WHERE	    JobID = :jobID
					ORDER BY    rh.ModifiedDate DESC
					LIMIT	    1
				    ) = 'Yes'
			    THEN    TRUE
			
			    ELSE    FALSE
			    
			END AS allowed    
	    ";
	
	$values = ["jobID" => $data["jobID"], "type" => $data["type"]];
	
	$result = $this->query($this->conn, $q, $values);
	
	return (isset($result[0]["allowed"]) ? $result[0]["allowed"] : false);
	
    }
    
    
    
    public function getRAStatus($jobID, $type) {
	
	$q = "	SELECT	    rs.RAStatusID,
			    rs.RAStatusName
		
		FROM	    ra_history AS rh
		
		LEFT JOIN   ra_status AS rs ON rh.RAStatusID = rs.RAStatusID
		
		WHERE	    rs.AuthorisationTypeID =	(
							    SELECT  at.AuthorisationTypeID
							    FROM    authorisation_types AS at
							    WHERE   at.AuthorisationTypeName LIKE :type
							    LIMIT   1
							)
			    AND	rh.JobID = :jobID
		
		ORDER BY    rh.ModifiedDate DESC
		
		LIMIT	    1
	     ";
	
	$values = ["type" => $type, "jobID" => $jobID];
	
	$result = $this->query($this->conn, $q, $values);
	
	return (count($result) > 0) ? $result[0] : null;
	
    }
    
    
    
    public function getPendingRA($jobID) {
	
	$q = "	SELECT	CASE
			    WHEN    (
					SELECT		rs.Final
					FROM		job
					LEFT JOIN	ra_status AS rs ON job.RAStatusID = rs.RAStatusID
					WHERE		job.JobID = :jobID
				    ) = 'No'
			    THEN    TRUE
			    ELSE    FALSE
			END AS pending
	     ";
	$values = ["jobID" => $jobID];
	$result = $this->query($this->conn, $q, $values);
	return ($result[0]["pending"] ? "true" : "");
	
    }
    
    
    
    public function getLastInsertID() {
	
	$q = "SELECT LAST_INSERT_ID() AS id";
	$result = $this->query($this->conn, $q);
	$this->controller->log($result);
	return isset($result[0]["id"]) ? $result[0]["id"] : null;
	
    }
    
    
    
}
?>