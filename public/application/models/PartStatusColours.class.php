<?php

require_once('CustomModel.class.php');
require_once('TableFactory.class.php');

/**
 * Description
 *
 * This class is used for handling database actions of PartStatusColour Page in Lookup Tables section under System Admin
 *
 * @author      Andris Polnikovs <a.polnikovs@gmail.com>
 * @version     1.0 
 * @created     21/06/2013
 */
class PartStatusColours extends CustomModel {

    public function __construct($controller) {

        parent::__construct($controller);

        $this->conn = $this->Connect($this->controller->config['DataBase']['Conn'], $this->controller->config['DataBase']['Username'], $this->controller->config['DataBase']['Password']);
        $this->SQLGen = $this->controller->loadModel('SQLGenerator');
        $this->fields = [
            
           
          "PartStatusID",
          "SpPartStatusColourID",
          "ServiceProviderID",
          "Colour",
          "TextColour"
            
           
            
        ];
    }

    public function insertPartStatusColour($P, $spid) {
        $P["ServiceProviderID"] = $spid;
        
        $id = $this->SQLGen->dbInsert('sp_part_status_colour', $this->fields, $P, true, true);
        return $id;
    }

    public function updatePartStatusColour($P, $spid) {
        $P["ServiceProviderID"] = $spid;
        
        $id = $this->SQLGen->dbUpdate('sp_part_status_colour', $this->fields, $P, "SpPartStatusColourID=" . $P['SpPartStatusColourID'], true);
    }

    public function getPartStatusColourData($id) {
        $sql = "select  ps.PartStatusID,sc.Colour,sc.SpPartStatusColourID,sc.TextColour from part_status ps 
                left join sp_part_status_colour  sc on sc.PartStatusID=ps.PartStatusID
                where ps.PartStatusID=$id";
        $res = $this->query($this->conn, $sql);
        return $res[0];
    }

   

   
    

}

?>