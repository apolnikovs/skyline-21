<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SQLGenerator
 *
 * @author Andris Polnikovs a.polnikovs@gmail.com
 */
class SQLGenerator extends CustomModel {

    private $conn;

    public function __construct($controller) {

        parent::__construct($controller);

        $this->conn = $this->Connect($this->controller->config['DataBase']['Conn'], $this->controller->config['DataBase']['Username'], $this->controller->config['DataBase']['Password']);

        $this->debug = true;
    }

    //inserting row in database
    //$d=created,modified times users default yes
    //$a=if true will use $v as post data, ie looking for array keys mathing fieldnames instead of values array
    /**
     * Description of SQLGenerator
     *
     * @author Andris Polnikovs a.polnikovs@gmail.com
     * @param string $t table name
     * @param array  $f field names
     * @param array  $v values 
     * @param bool   $d if true will add userdata to query 
     * @param bool   $d if true will use values as key=>value
     */
    public function dbInsert($t, $f, $v, $d = true, $a = false) {
        $this->controller->log($f, "SQLGEN_______");
        $sep = '';
        $val = '';
        $col = '';
        $tok = '';
        if ($d) {
            if($a){
            $f[] = "CreatedUserID";
            $f[] = "CreatedDate";
            $f[] = "ModifiedUserID";
            

            $v['CreatedUserID'] = isset($this->controller->user->UserID) ? $this->controller->user->UserID : null;
            $v['CreatedDate'] = date('Y-m-d H:i:s');
            $v['ModifiedUserID'] = isset($this->controller->user->UserID) ? $this->controller->user->UserID : null; 
            }else{
            $f[] = "CreatedUserID";
            $f[] = "CreatedDate";
            $f[] = "ModifiedUserID";

            $v[] = isset($this->controller->user->UserID) ? $this->controller->user->UserID : null;
            $v[] = date('Y-m-d H:i:s');
            $v[] = isset($this->controller->user->UserID) ? $this->controller->user->UserID : null;
            }
        }

        if (!$a) {
            for ($i = 0; $i < sizeof($f); $i++) {
                $col.=$sep . $f[$i];
                $tok.=$sep . ":" . $f[$i];
                $val[$f[$i]] = $v[$i];
                $sep = ",";
            }
        } else {
            foreach ($f as $ff) {

                $col.=$sep . $ff;
                $tok.=$sep . ":" . $ff;
                $vv = isset($v[$ff]) ? $v[$ff] : "No";
                $val[$ff] = $vv;
                $sep = ",";
            }
        }

        $sql = "INSERT INTO $t (" . $col . ") VALUES (" . $tok . ")";
        if ($this->debug) {
            $this->controller->log($sql, "SQLGEN_______");
            $this->controller->log($val, "SQLGEN_______");
        }
        $this->Execute($this->conn, $sql, $val);
        return $this->conn->lastInsertId();
    }

//inserting row in database
    //$t=table string
    //$f=fields array (columns)
    //$v=values array
    //$k=where
    //$a=boolean if true use v as post data
    public function dbUpdate($t, $f, $v, $k, $a = false) {
        $sep = '';
        $val = '';
        $col = '';
        $tok = '';
        if($a){
        $f[] = "ModifiedUserID";
        $f[] = "ModifiedDate";
        $v['ModifiedUserID'] = isset($this->controller->user->UserID) ? $this->controller->user->UserID : null;
        $v['ModifiedDate'] = date('Y-m-d H:i:s');
        }else{
            $f[] = "ModifiedUserID";
            $f[] = "ModifiedDate";
        $v[] = isset($this->controller->user->UserID) ? $this->controller->user->UserID : null;   
        $v[] = date('Y-m-d H:i:s');
        }
        if (!$a) {
            for ($i = 0; $i < sizeof($f); $i++) {

                $tok.=$sep . $f[$i] . "=:" . $f[$i];
                $val[$f[$i]] = $v[$i];
                $sep = ",";
            }
        } else {
            foreach ($f as $ff) {

                $col.=$sep . $ff;
                $tok.=$sep . $ff."=:" . $ff;
                $vv = isset($v[$ff]) ? $v[$ff] : "No";
                $val[$ff] = $vv;
                $sep = ",";
            }
        }
        $sql = "UPDATE $t SET " . $tok . " WHERE $k";
        if ($this->debug) {
            $this->controller->log($sql, "SQLGEN_______");
            $this->controller->log($val, "SQLGEN_______");
        }
        $this->Execute($this->conn, $sql, $val);
    }

    public function dbMakeInactive($t, $k) {
        $sql = "UPDATE $t SET Status='In-active' WHERE $k";
        $this->Execute($this->conn, $sql);
    }

}

?>
