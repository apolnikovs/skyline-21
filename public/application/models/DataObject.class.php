<?php

/**
 * Description of Timeline
 *
 * @author Simon Tsang <s.tsang@pccsuk.com>
 */

require_once('TableFactory.class.php');

class DataObject extends CustomModel {
    private $conn;
    
    public static $default;
    
    /*
     * @param PDO $conn 
     * @param int $currentUser
     */
    public function __construct($conn, $currentUser=null) {
        $this->conn = $conn;
        
        if(self::$default == null && $currentUser != null){
            $user_info = $this->collectByID('user', "UserID = '{$currentUser}'");  
        
            self::$default = $this->collectByID('general_default', "BrandID = '".$user_info['DefaultBrandID']."'");   
        }else{
            self::$default = $this->collectByID('general_default', "GeneralDefaultID = 1"); 
        }

    }
    
    /*
     * 
     */
    public static function GetDefaults(){
        return self::$default;
    }
    
    
    /*
     * @return array
     */
    public function getFields($table){
        $sql = "SHOW COLUMNS FROM $table";
        
        $query = $this->conn->query( $sql );;
        $query->setFetchMode( PDO::FETCH_ASSOC ); #FETCH_NUM   
        
        return $query->fetchAll();             
    }
    
    
    /**
     * 
     * collect fn gets the all the data from database table
     * 
     * @param string $table database table name
     * @param array|string if array then $where1 AND $where2
     * @param string $select    
     * @param string 
     * 
     * @return array
     * 
     * @author Simon Tsang s.tsang@pccsuk.com
     ********************************************/
     public function collect($table, $where=null, $select='*', $orderBy=null){       
        $where_clause = '';
        
        if( is_array($where) ){
            foreach($where as $field => $value){
                $where_clause .= ( $where_clause=='' ? 'WHERE ' : ' AND ' ) . "$field = '$value'";
            }
        }else if(is_string($where)){
            if( stristr(strtolower( $where ), 'where') )
                $where_clause = $where;
            else
                $where_clause = ' WHERE ' . $where;
        }
        
        if( is_string( $orderBy ) ){ 
            $lc = strtolower( $orderBy );
            $orderBy = ( strstr($lc, 'order by')  ? '' : 'ORDER BY ' ) . $orderBy;
        }
        
        $sql = "SELECT $select FROM $table $where_clause $orderBy"; 
        #echo 'DataObject->collect :' . $sql;

          
        $query = $this->conn->query( $sql );
        $query->setFetchMode( PDO::FETCH_ASSOC ); #FETCH_NUM   
        
        return $query->fetchAll();          
        
    }    

    /**
     * 
     * collect fn gets the all the data from database table
     * 
     * @param string $table database table name
     * @param array $where name array( 'tabename' => 'value') 
     * @param string $select    
     * @return array
     * 
     * @author Simon Tsang s.tsang@pccsuk.com
     ********************************************/
    public function collectByID($table, $where=null, $select='*', $orderBy=null){       
        $where_clause = '';
        
        if( is_array($where) ){
            foreach($where as $field => $value){
                $where_clause .= ( $where_clause=='' ? 'WHERE ' : ' AND ' ) . "$field = '$value'";
            }
        }else if(is_string($where)){
            if( stristr(strtolower( $where ), 'where') )
                $where_clause = $where;
            else
                $where_clause = ' WHERE ' . $where;
        }
            
        if( is_string( $orderBy ) ){ 
            $lc = strtolower( $orderBy );
            $orderBy = ( strstr($lc, 'order by')  ? '' : 'ORDER BY ' ) . $orderBy;
        }        
        
        $sql = "SELECT $select FROM $table $where_clause $orderBy LIMIT 0, 1"; 
        #echo('DataObject->collectByID :' . $sql);

          
        $query = $this->conn->query( $sql );
        $query->setFetchMode( PDO::FETCH_ASSOC ); #FETCH_NUM   
        
        return $query->fetch();          
        
    }    
    
    /*
     * @tutorial $this->enum_select($table, $field);
     */
    public function enum_select( $table , $field ){
        $sql = " SHOW COLUMNS FROM `$table` LIKE '$field' ";
        
        $query = $this->conn->query( $sql );
        $query->setFetchMode( PDO::FETCH_ASSOC ); #FETCH_NUM   
        
        $row = $query->fetch();   

        #extract the values
        #the values are enclosed in single quotes
        #and separated by commas
        $regex = "/'(.*?)'/";

        preg_match_all( $regex , $row['Type'], $enum_array );
        $enum_fields = $enum_array[1];
        
        return( $enum_fields );
}
}
    
?>
