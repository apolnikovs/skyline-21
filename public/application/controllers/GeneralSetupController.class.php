<?php
/**
 * Description of GeneralSetupController
 *
 * @author      Simon Tsang <tsang.kuen@gmail.com>
 * 
 * @copyright   2012 PC Control Systems Ltd
 * 
 * Changes
 * Date        Version Author               Reason
 * ??/??/2012  1.00    Simon Tsang          Initial Version      
 * 26/06/2012  1.01    Andrew J. Williams   Added General Defaults functionality
 * 24/07/2012  1.02    Brian Etherington    Changed case of class name to GeneralSetupController
 * 01/07/2013          Thirumalesh Bandi      Added page title and page description dynamically for every page.
 ******************************************************************************/

require_once('CustomSmartyController.class.php');

class GeneralSetupController extends CustomSmartyController {
    
    public  $config,
            $session,
            $skyline,
            $messages;
    public $lang = 'en';
    public $page;
    public $statuses  = array(
                     
                     0=>array('Name'=>'Active', 'Code'=>'Active'),
                     1=>array('Name'=>'In-Active', 'Code'=>'In-active')
                    
                     );
    public  $SkylineBrandID=1000;
    
        public function __construct() { 
        
        parent::__construct(); 
        
       /* ==========================================
         * Read Application Config file.
         * ==========================================
        */
        
        $this->config = $this->readConfig('application.ini');
        
       /* ==========================================
         *  Initialise Session Model
         * ==========================================
         */
               
        $this->session = $this->loadModel('Session'); 
        
        /* ==========================================
         *  Initialise Messages Model
         * ==========================================
         */
               
        $this->messages = $this->loadModel('Messages'); 
        
        //$this->smarty->assign('_theme', 'skyline');
        
        if(isset($this->session->UserID)) {

            $user_model = $this->loadModel('Users');
            $this->user = $user_model->GetUser($this->session->UserID);
	    
	    $this->smarty->assign('loggedin_user', $this->user);
            
            $this->smarty->assign('name',$this->user->Name);
            $this->smarty->assign('session_user_id', $this->session->UserID);
            
            if($this->user->BranchName)
            {
                $this->smarty->assign('logged_in_branch_name', " - ".$this->user->BranchName." ".$this->config['General']['Branch']." ");
            }
            else
            {
                $this->smarty->assign('logged_in_branch_name', "");
            }
            
            if($this->session->LastLoggedIn)
            {
                $this->smarty->assign('last_logged_in', " - ".$this->config['General']['LastLoggedin']." ".date("jS F Y G:i", $this->session->LastLoggedIn));
            }
            else
            {
                $this->smarty->assign('last_logged_in', '');
            } 
            
            $topLogoBrandID = $this->user->DefaultBrandID;
            $this->smarty->assign('_theme', $this->user->Skin);

        } else {
            
           $topLogoBrandID = isset($_COOKIE['brand'])?$_COOKIE['brand']:0;
            $this->smarty->assign('session_user_id','');
            $this->smarty->assign('_theme', 'skyline');
           // $this->smarty->assign('name','');
           // $this->smarty->assign('last_logged_in','');
           // $this->smarty->assign('logged_in_branch_name', '');
            $this->redirect('index',null, null);

        } 
        
        if($topLogoBrandID)
        {
            $skyline = $this->loadModel('Skyline');
            $topBrandLogo = $skyline->getBrand($topLogoBrandID);

            $this->smarty->assign('_brandLogo', (isset($topBrandLogo[0]['BrandLogo']))?$topBrandLogo[0]['BrandLogo']:'');
        }
        else
        {
            $this->smarty->assign('_brandLogo', '');
        }
        
        $this->smarty->assign('showTopLogoBlankBox', false);
        
         if (isset($this->session->lang)) {
            $this->lang = $this->session->lang;
        }   
    }
    
    public function indexAction( $args ){
        
    }
    
    /**
     * Description
     * 
     * This method is used to process the data i.e fething, updating and inserting.
     * 
     * @param array $args It contains modelname as first element where second element as process type
     * @return void It prints json encoded result where datatable using this data.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com> 
     **************************************************************************/
     public function ProcessDataAction($args)
     {
         #$this->log( var_export($args, true) );
            $modelName = isset($args[0])?$args[0]:'';
            $type      = isset($args[1])?$args[1]:'';
         
            if($modelName && $modelName!='')
            {
                $model  = $this->loadModel($modelName);
                
                if($type=='fetch')
                {
                    $_POST['firstArg']  = isset($args[2])?$args[2]:'';
                    $_POST['secondArg'] = isset($args[3])?$args[3]:'';                    
                    $result = $model->fetch($_POST);
                }
                else
                {
                    $result = $model->processData($_POST);
                }    
                echo json_encode( $result );
           }
            return;
     }
    
    public function accessPermissionsAction( $args ){
        
    } 
   
    /**
     * generalDefaultsAction
     *  
     * Create a customer
     * 
     * @param array $args   Array of arrguments to be passed :
     *                      First element action (insert or update)
     *                      Second element Primary Key.
     * 
     * @return integer  ID of new customer or NULL if not vreated 
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    public function generalDefaultAction ( $args ) {
        $Skyline = $this->loadModel('Skyline');
        $general_default_model = $this->loadModel("GeneralDefault");
        
        $functionAction = isset($args[0])?$args[0]:'';
        $selectedRowId = isset($args[1])?$args[1]:''; 
        
        $Skyline = $this->loadModel('Skyline');
          
        /* 
         * If the action is insert then we are getting only skyline brand since 
         * user(i.e super admin) can create actions only for skyline (Of course 
         * others can copy this action if they want)
         */
        if($functionAction=="insert" && $selectedRowId=='') {
            $brands = $Skyline->getBrand(1000);
        } else if($functionAction=="insert" && $selectedRowId!='') {
            $brands = $Skyline->getBrand('',1000);
        } else {                                                                /* Getting brands (logged in user can see brands which are assinged to him/her )... */
            $brands = $Skyline->getBrand();
        }
        
        $this->smarty->assign('brands', $brands); 
        $this->smarty->assign('statuses', $this->statuses);
        $this->smarty->assign('SupderAdmin', $this->user->SuperAdmin);          /* This value should be from user class */
        $this->smarty->assign('userPermissions', $this->user->Permissions);     /* This value should be from user class */
        $this->smarty->assign('userBrands', $this->user->Brands);               /* This value should be from user class */
        $this->smarty->assign('accessDeniedMsg', $this->messages->getError(1023, 'default', $this->lang));

        $accessErrorFlag = false;

        $this->page = $this->messages->getPage('generalDefault', $this->lang);
        
           //Getting Page title & Description from database
            $model = $this->loadModel('GroupHeadings');        
            $groupHedingResults = $model->getHeadingPageDetails('generalDefault');

            // Over write the page title & description
            $page['Text']['page_title'] = isset($groupHedingResults[0])?$groupHedingResults[0]:$page['Text']['page_title'];
            $page['Text']['description'] = isset($groupHedingResults[1])?$groupHedingResults[1]:$page['Text']['description'];

        
        $this->smarty->assign('page', $this->page);
        
        switch( $functionAction ) {
            case 'insert':
                $args['GeneralDefaultID'] = $selectedRowId;                     /* If a row has been selected then place it in the ID field */
                if($args['GeneralDefaultID']!='')  {                            /* a row has been selected so we are copying */
                    $general_default_model = $this->loadModel('GeneralDefault'); 
                    $datarow = $general_default_model->fetchRow($args);
                    $datarow['BrandID'] = '';
                    $datarow['GeneralDefaultID'] = '';                          /* We want to create a new itemk from this data so blank the key ID */
                    $datarow['Status'] = 'Active';
                    $datarow['ModifiedUserID'] = $this->user->UserID;
                    $datarow['ModifiedDate'] = date("Y-m-d H:i:s");
                } else {
                    $datarow = array(
                                     'GeneralDefaultID' => '',
                                     'IDNo' => '',
                                     'Category' => '',
                                     'DefaultName' => '',
                                     'Default' => '',
                                     'Description' => '',
                                     'BrandID' => '',
                                     'CreatedDate' => date("Y-m-d H:i:s"),
                                     'Status' => 'Active',
                                     'ModifiedUserID' => $this->user->UserID,
                                     'ModifiedDate' => date("Y-m-d H:i:s")
                                    );
                }

                //Checking user permissons to display the page.
                if($this->user->SuperAdmin)
                {
                    $accessErrorFlag = false;
                }
                else if(isset($this->user->Permissions['AP7001']) || isset($this->user->Permissions['AP7002']))
                {
                    if($datarow['CountyCode']=='')
                    {
                        $accessErrorFlag = true;
                    }
                }

                $this->smarty->assign('datarow', $datarow);
                $this->smarty->assign('form_legend', $this->page['Text']['insert_page_legend']);
                $this->smarty->assign('accessErrorFlag', $accessErrorFlag);

                $htmlCode = $this->smarty->fetch('generalDefaultForm.tpl');

                echo $htmlCode;

                break;
            
            case 'update':
                $args['GeneralDefaultID'] = $selectedRowId;
                
                $general_default_model = $this->loadModel('GeneralDefault');

                $datarow = $general_default_model->fetchRow($args);
                
                 //If the super admin updating existing record of skyline he can not change this brand to other brand so we are only fetching skyline for the brand dropdown in the update page.
                if($datarow['BrandID']==1000)
                {
                        $brands = $Skyline->getBrand(1000);
                        $this->smarty->assign('brands', $brands); 
                }
                
                $datarow['ModifiedUserID'] = $this->user->UserID;               /* If we save we want the modified by user field to be the current user */
                $datarow['ModifiedDate'] = date("Y-m-d H:i:s");                       /* And the date to be todays date */
                
                if($this->user->SuperAdmin) {
                    $accessErrorFlag = false;
                } else if(isset($this->user->Permissions['AP7001']) || isset($this->user->Permissions['AP7002'])) {
                    if(!isset($this->user->Brands[$datarow['BrandID']])) {
                        $accessErrorFlag = true;
                    } else {
                        $accessErrorFlag = false;
                    }
                } else {
                    $accessErrorFlag = true;
                }
                $this->smarty->assign('accessErrorFlag', $accessErrorFlag);

                $this->smarty->assign('form_legend', $this->page['Text']['update_page_legend']);

                $this->smarty->assign('datarow', $datarow);

                $htmlCode = $this->smarty->fetch('generalDefaultForm.tpl');

                echo $htmlCode;

                break;

            default:

                $this->smarty->display('generalDefault.tpl');

            }
        
    }
    
    
}

?>
